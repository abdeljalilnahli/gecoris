#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 18:14:25 2021

@author: rc
"""

import glob, os
import pandas as pd
import numpy as np
from matplotlib import dates
formatter = dates.DateFormatter('%m-%y')  
import matplotlib.pyplot as plt
from scipy import interpolate
from scipy.spatial.distance import cdist
from gecoris import insarUtils, geoUtils, ioUtils, plotUtils, insar


#%% inputs:

station_log = '/data/GUDS/CR_Prievidza/CR_Prievidza.csv'
stacks_log = '/data/GUDS/CR_Prievidza/input_stacks_Prievidza.csv'
process_dir = '/data/GUDS/CR_Prievidza/gecoris'
#out_dir = '/data/GUDS/CR_Prievidza/insar/'

stations, refl_flag = ioUtils.load_stations(station_log, process_dir)

#%% do insar analysis for all stacks:
insar_parms = ioUtils.prepare_insar_parms(station_log, stacks_log, process_dir)
for parm in insar_parms:
    insar.insar(parm)

#%% plot individual reflectors:
for i in range(len(stations)):
    for parm in insar_parms:
        hdf_file = parm['outDir'] + 'insar_' + parm['stackId'] + '.hdf5'
        stations[i].add_insar_ts(hdf_file, parm['stackId'])
    #stations[i].add_insar_ts(asc_hdf, asc_id)
    #stations[i].add_insar_ts(dsc_hdf, dsc_id)
    stations[i].plot_insar_ts(out_dir = parm['outDir'])
    #%% decomposition:
    stations[i].decomp_insar_ts()
    stations[i].plot_decomp_ts(out_dir = parm['outDir'])
        

#%% decomposition:
stacks = stations[0].stacks

# take shorthest stack as reference:
ref_idx = np.argmin([len(s['insar']['dates']) for s in stacks])
ref_dates = stacks[ref_idx]['insar']['dates']
# allocate:
n_stacks = len(stacks)
n_dates = len(ref_dates)
los = np.zeros((n_dates, n_stacks))
sig_los = np.zeros_like(los)
inc = np.zeros(n_stacks)
head = np.zeros(n_stacks)
# prepare decomposition arrays
for i in np.arange(n_stacks):
    d = stacks[i]['insar']['dates']
    # find closest dates:
    d_idx = np.argmin(np.abs(d[:, np.newaxis] - ref_dates), axis=0)
    los[:,i] = stacks[i]['insar']['ts'][d_idx]
    sig_los[:,i] = stacks[i]['insar']['sigma_ts'][d_idx]
    inc[i] = stacks[i]['zas'][0]
    head[i] = stacks[i]['zas'][1]

stations[0].vh, stations[0].sig_vh = insarUtils.decomp(los, sig_los, inc, head)

plt.plot(vh[0])
plt.plot(vh[1])
 
#%% load reflectors:
stationLog = '/data/GUDS/CR_Prievidza/CR_Prievidza.csv'
logDir = '/data/GUDS/CR_Prievidza/gecoris'
insarHDF = '/data/GUDS/CR_Prievidza/insar/insar_ASC175.hdf5'
#stn = pd.read_csv('/home/rc/Delft/RS/InSAR/CR_Prievidza.csv')

# if stationLog.lower().endswith('.json'):
#     stations = ioUtils.load_station_log(stationLog)
# elif stationLog.endswith('.csv'):
#     stations = ioUtils.fromCSV(stationLog)
# else:
#     print('Unknown station log file format. Use .json or .csv.')
#     raise
# # load analysed .json logs:
# refl_flag = True
# try:
#     logs = glob.glob(logDir + os.sep +"*.json")
#     for i in range(len(stations)):
#         inJSON = [q for q in logs 
#                       if stations[i].id+'.json' in q.split(os.sep)[-1]]
#         stations[i] = ioUtils.fromJSON(inJSON[0])
# except:
#     print('No refl. analysis performed for reflectors in specified dir.')
#     refl_flag = False


station_log = '/data/GUDS/CR_Prievidza/CR_Prievidza.csv'
process_dir = '/data/GUDS/CR_Prievidza/gecoris'
out_dir = '/data/GUDS/CR_Prievidza/insar/'

stations, refl_flag = ioUtils.load_stations(station_log, process_dir)

# load InSAR 
try:
    data = insarUtils.openHDF(insarHDF)
    plh_all = data['psc2/plh'][:]
    ts_all = data['network2/ps_displ'][:]/data['network2'].attrs['m2ph']*1e3
    var_all = data['network2/ps_var'][:]
    refIdx = data['network2'].attrs['refIdx']
    ts_all = np.insert(ts_all, refIdx, 0, axis=0)
    var_all = np.insert(var_all, refIdx, 0)
    dates_all = data['network2'].attrs['dates']
    std_res_all = data['network2/stdRes'][:]
    VC_mean = (np.sqrt(data['network2/VC_mean'][:]) /
               -data['network2'].attrs['m2ph']*1e3)
except:
    print('No insar HDF data found.')
    raise

stackId = 's1_asc175'

#%% iterate reflectors:
for i in range(len(stations)):
    # extract TS:
    plh = stations[i].get_plh()[0]
    # take index of closest PS:
    idx = np.argmin(cdist(plh_all[:,:2], plh[None,:2]))
    ts = ts_all[idx,:]
    # TODO: add warning if too far
    if refl_flag:
        try:
            stack_idx = stations[i].getStackIdx(stackId)
        except:
            print('Reflectivity analysis not performed for reflector' + 
                  stations[i].id + 'and stack' + stackId)
            raise
        good_idx = np.intersect1d(
            dates.datestr2num(
            stations[i].stacks[stack_idx]['data']['acqDate'][
                stations[i].stacks[stack_idx]['goodIdx']
                ]), 
            dates_all, return_indices=True)[-1]
        # load sig_phi predicted by SCR
        sig_phi_SCR = stations[i].stacks[stack_idx]['sigPhi_SCR']*np.sqrt(2)
        # remove outliers:
        plot_dates = dates_all[good_idx]
        plot_ts = ts[good_idx]
        VC = VC_mean[good_idx]
    else:
        plot_dates = dates_all
        plot_ts = ts
        VC = VC_mean
    # stds:
    sig = np.sqrt(var_all[idx])*VC
    std_res = std_res_all[idx]    
    
    # plot:
    plotUtils.plot_insar_ts(plot_dates, plot_ts, sig, 
                            stations[i].id, stackId, out_dir = out_dir)
    # fig, ax = plt.subplots(figsize=(9,4),dpi=120)
    # #plt.plot(plot_dates, plot_ts, label = stackId, fmt='-^')
    # #plt.plot(plot_dates, plot_ts,'-^', label = stackId)
    # ax.errorbar(plot_dates, plot_ts, fmt='-^', yerr=2.5*sig,
    #             ecolor='gray', label=stackId, color='C0')
    # ax.set_ylabel('LOS displacement [mm]')
    # start, end = ax.get_xlim()
    # stepsize = (end+15-start)/10
    # ax.xaxis.set_ticks(np.arange(start, end+15, stepsize))
    # ax.xaxis.set_major_formatter(formatter)
    # ax.xaxis.set_tick_params(rotation=30, labelsize=10)
    # ax.grid(alpha = 0.35) 
    # ax.set_title(stations[i].id,fontsize = 14, fontweight='bold')
    # ax.legend(loc = 'best', fontsize=14)
    

#%%

(ts_asc,insarDates_asc,_,_) = loadCSV_insar(
    '/home/rc/Delft/RS/InSAR/CR_Prievidza_ASC175_free.csv')
masterDate_asc = '20200301'
masterIdx_asc = 1
VC_asc = np.genfromtxt('/home/rc/Delft/RS/InSAR/VC_ASC175.csv', delimiter=',')
inc_asc = 0.6575234049996417
heading_asc = -1.7496260659567204

redundIdx = 38
#ts_asc = np.delete(np.array(ts_asc),redundIdx,1)
#ts_asc.drop(redundIdx)
#insarDates_asc = np.delete(insarDates_asc,redundIdx)
#VC_asc = np.delete(VC_asc,redundIdx)

(ts_dsc,insarDates_dsc,_,_) = loadCSV_insar(
    '/home/rc/Delft/RS/InSAR/CR_Prievidza_DSC51_free_fix.csv')
masterDate_asc = '20200305'
#masterIdx_dsc = 2
masterIdx_dsc = 2
VC_dsc = np.genfromtxt('/home/rc/Delft/RS/InSAR/VC_DSC51.csv', delimiter=',')
inc_dsc = 0.7311387147
heading_dsc = 1.7365975942846659

#%%
n = 5
stns = [6,0,1,3,4]
#stns = [6,5,4,3,2,1,0]
fig, ax = plt.subplots(n,2,figsize=(10,n*2),dpi=120,sharex=True)
fig.subplots_adjust(hspace=0)
for i in np.arange(n):
    stnId = stn['ID'][stns[i]]
    lon = stn['LONGITUDE'][stns[i]]
    lat = stn['LATITUDE'][stns[i]]

    # extract PS:
    t_asc,LOS_asc,COH_asc = extractPS(ts_asc,insarDates_asc,lon,lat)
    t_dsc,LOS_dsc,COH_dsc = extractPS(ts_dsc,insarDates_dsc,lon,lat)
    
    # fix master idx:
    LOS_asc -= LOS_asc[masterIdx_asc]
    LOS_dsc -= LOS_dsc[masterIdx_dsc]
    
    # plot:
    #ax[i].plot(t_asc,LOS_asc,'-^',label='ASC')
    ax[i][0].errorbar(t_asc,LOS_asc,fmt='-^',yerr=2*VC_asc,ecolor='gray',
                   label='ASC',color='C0')
    #ax[i].plot(t_dsc,LOS_dsc,'-o',label='DSC')
    ax[i][0].errorbar(t_dsc,LOS_dsc,fmt='-o',yerr=2*VC_dsc,ecolor='gray',
                   label='DSC',color='C1')
    ax[i][0].set_ylim(-10,10)
    #ax[i][0].legend(fontsize=13)
    ax[i][0].grid(alpha = 0.3)
    # errorbars!
    
    # remove redundant:
    LOS_asc = np.delete(LOS_asc,redundIdx)
    t_asc = np.delete(t_asc,redundIdx)
    vc_asc = np.delete(VC_asc,redundIdx)
    
    # fit spline:
    MDLarr = np.empty(4)
    for splines in np.arange(1,5):
        #q = splines*3
        #knots = t_asc[::int(np.ceil(len(t_asc)/splines))][1:]
        #tck = interpolate.LSQUnivariateSpline(t_asc, LOS_asc, 
        #                                      knots, w=1/vc_asc)
        #ynew = tck(t_asc)
        #res = LOS_asc-ynew
        #MDL = np.log(len(t_asc))*q/len(t_asc)+np.log(np.sum(res**2))
        _,_,MDL = geoUtils.regularSplines(t_asc, LOS_asc, vc_asc, splines)
        MDLarr[splines-1] = MDL
    ynew,res,_ = geoUtils.regularSplines(t_asc, LOS_asc, vc_asc, 
                                      np.argmin(MDLarr)+1)
    ax[i][0].plot(t_asc,ynew)
    print('ASC_spline=')
    print(np.argmin(MDLarr)+1)
    print(np.std(res))
    
    # fit spline:
    MDLarr = np.empty(4)
    for splines in np.arange(1,5):
        _,_,MDL = geoUtils.regularSplines(t_dsc, LOS_dsc, VC_dsc, splines)
        MDLarr[splines-1] = MDL
    ynew,res,_ = geoUtils.regularSplines(t_dsc, LOS_dsc, VC_dsc, 
                                      np.argmin(MDLarr)+1)
    ax[i][0].plot(t_asc,ynew)
    print('DSC_spline=')
    print(np.argmin(MDLarr)+1)
    print(np.std(res))
    #tck = interpolate.splrep(t_dsc, LOS_dsc, w=1/VC_dsc, s=q)
    #ynew = interpolate.splev(t_dsc, tck, der=0)
    #ax[i][0].plot(t_asc,ynew)
    #res = LOS_dsc-ynew
    #print('DSC_spline=')
    #print(np.std(res))    
    # compute STD:
    #delta_t = (t_asc - t_asc[masterIdx_asc])/365.25
    #n = delta_t.shape[0]
    #A = np.vstack((np.ones(n), delta_t, np.sin(
    #            2*np.pi*delta_t), np.cos(2*np.pi*delta_t)-1)).T
    #res = LOS_asc - A@np.linalg.inv(A.T@A)@A.T@LOS_asc
    #print('ASC=')
    #print(np.std(res))
    #delta_t = (t_dsc - t_dsc[masterIdx_dsc])/365.25
    #n = delta_t.shape[0]
    #A = np.vstack((np.ones(n), delta_t, np.sin(
    #            2*np.pi*delta_t), np.cos(2*np.pi*delta_t)-1)).T
    #res = LOS_dsc - A@np.linalg.inv(A.T@A)@A.T@LOS_dsc
    #print('DSC=')
    #print(np.std(res))
    # VCM:
    #Qy = np.diag([VC_asc**2,VC_dsc**2])
    
    # decompose:
    x,sig_x = decomp(LOS_asc,LOS_dsc,inc_asc,inc_dsc,heading_asc,heading_dsc,
               VC_asc,VC_dsc)
    #ax[i][1].plot(t_asc,x[0],'-^',label='UP/DOWN')
    ax[i][1].errorbar(t_asc,x[0],fmt='-^',yerr=2*sig_x[0],ecolor='gray',
               label='UP/DOWN',color='C2')
    #ax[i][1].plot(t_asc,-x[1],'-o',label='EAST/WEST')
    ax[i][1].errorbar(t_dsc,-x[1],fmt='-o',yerr=2*sig_x[1],ecolor='gray',
                   label='EAST/WEST',color='C3')
    
    ax[i][1].set_ylim(-10,10)
    #ax[i][1].legend(fontsize=13)
    ax[i][1].grid(alpha = 0.3)
    
    # date labels:
    for aq in ax[i]:
        # stn id annotation:
        aq.annotate(stnId,xy=(aq.get_xlim()[0]+10,7),
            fontSize = 14,fontweight='bold')
        # date labels:
        start, end = aq.get_xlim()
        stepsize = (end-start)/11
        aq.xaxis.set_ticks(np.arange(start, end, stepsize))
        aq.xaxis.set_major_formatter(formatter)
        aq.xaxis.set_tick_params(rotation=30, labelsize=12)
        aq.yaxis.set_tick_params(labelsize=12)
    
    # plot legend if first:
    if i == 0:
        ax[i][0].legend(fontsize=16,bbox_to_anchor=(0.05, 1.35),
                        loc='upper left',ncol=2,prop={'weight':'bold',
                                                      'size':16})
        ax[i][1].legend(fontsize=16,bbox_to_anchor=(-0.15, 1.35),
                        loc='upper left',ncol=2,prop={'weight':'bold',
                                                      'size':16})
    
    # common y label:
    fig.text(0.04, 0.5, 'DISPLACEMENT [mm]', va='center', rotation='vertical',
             fontsize=16)    
    
    #plt.tight_layout()
    plt.savefig('/home/rc/Delft/RS/InSAR/HRD_InSAR.png',bbox_inches='tight')

    