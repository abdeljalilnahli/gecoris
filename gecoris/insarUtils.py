#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""insarUtils

Module containing utilities for time series InSAR analysis

Copyright (C) 2021 by R.Czikhardt

Email: czikhardt.richard@gmail.com
Last edit: 4.3.2021

This file is part of GECORIS - Geodetic Corner Reflector (In)SAR Toolbox.

    GECORIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GECORIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GECORIS. If not, see <https://www.gnu.org/licenses/>.
"""

import os
import numpy as np
import scipy.stats as ss
import scipy.linalg as sl
from scipy.spatial.distance import pdist, cdist
from scipy.optimize import least_squares
import pandas as pd
import itertools
import h5py
from matplotlib import dates, tri
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
#
from gecoris import classes, geoUtils, radarUtils, s1Utils, ioUtils, plotUtils
#import glob
#import datetime
#import cv2

# default estimation bounds:
bounds = {'dH' : 20, # [m]
          'vel': 5,  # [mm/year]
          'seasonal': 2
          }

class kriging:
    def __init__(self, x, y, z, var_model):
        # prepare azimuth/range grid
        self.x = x
        self.y = y
        self.z = z
        self.var_model = var_model
        self.n = x.size
        self.model = 'gaussian'

    def cov_model(self, d, nugget = True):
        # parms = [psill, range, nugget]
        parms = self.var_model
        if self.model == 'gaussian':
            cv = (parms[0]+parms[2])*np.exp(-( (d**2) / ((parms[1]*4/7)**2) ) )
            if nugget:
                cv += np.kron(np.eye(d.shape[0]), parms[2])
        return cv

    def kriging_matrix(self, x, y, x0, y0):
        """construct Kriging system matrix and solve it:
        # for universal kriging with drift of order 1
        """
        # matrix for original points:
        n = x.size
        xy = np.column_stack((x, y))
        d = cdist(xy, xy, "euclidean")
        A = np.zeros((n + 3, n + 3))
        A[:n, :n] = self.cov_model(d)
        A[:n, n] = 1.0
        A[n, :n] = 1.0
        A[:n, n+1:] = 0.0001*xy # normalisation to avoid singular matrix
        A[n+1:, :n] = 0.0001*xy.T
        # matrix for interpolant:
        n0 = x0.size
        xy0 = np.column_stack((x0, y0))
        d = cdist(xy, xy0, "euclidean")
        A0 = np.zeros((n + 3, n0))
        A0[:n, :] = self.cov_model(d, nugget=False)
        A0[n, :] = 1
        A0[n+1:, :] = 0.0001*xy0.T
        # solve system:
        q = np.linalg.solve(A, A0)
        return q[:n,:]
    
    def interpolate(self, x0, y0, n_max = 100, max_d = 1e5):
        # iterate points:
        sol = np.zeros_like(x0)
        for i in np.arange(x0.size):
            # PS coords:
            x0_ = x0[i].copy()
            y0_ = y0[i].copy()
            # initialize:
            n_max = min(n_max, self.n)
            # use only max. neighbours up to max. distance
            dx = np.hypot(self.x - x0_, self.y - y0_)
            idx = np.argsort(dx[dx < max_d])
            x_ = self.x[idx[:n_max]]
            y_ = self.y[idx[:n_max]]
            z_ = self.z[idx[:n_max]]
            #kriging system matrix:
            A = self.kriging_matrix(x_, y_, x0_, y0_)
            # solution:
            sol[i] = A.T@z_
        return sol  
    
    def plot(self, sol, outFig = ''):
        fig, ax = plt.subplots(1,2,figsize=(10,5))
        raw = ax[0].scatter(self.y, self.x, c=self.z, s=5)
        ax[0].set_title(r'$\hat{v}_{PS}$ - residual phase [rad]',
                        fontsize=15)
        ax[0].yaxis.set_tick_params(labelsize=8, rotation = 90)
        ax[0].xaxis.set_tick_params(labelsize=8)
        ax[0].invert_yaxis()
        raw.set_clim(np.min(sol),np.max(sol))
        #fig.colorbar(raw, ax=ax[0])
        model = ax[1].scatter(self.y, self.x, c=sol, s=5)
        ax[1].set_title('APS [rad]',fontsize=15)
        ax[1].yaxis.tick_right()
        ax[1].yaxis.set_tick_params(labelsize=8, rotation = 90)
        ax[1].xaxis.set_tick_params(labelsize=8)
        ax[1].invert_yaxis()
        fig.subplots_adjust(wspace=0.3)
        #cbar_ax = fig.add_axes([0.92, 0.15, 0.03, 0.7])
        cbar_ax = fig.add_axes([0.47, 0.15, 0.03, 0.7])
        c = fig.colorbar(raw, cax = cbar_ax)
        c.ax.yaxis.set_tick_params(labelsize=14)
        #fig.colorbar(model, ax=ax[1])
        if outFig:
            plt.savefig(outFig, bbox_inches='tight')
            plt.close(fig)


class variogram:
    
    def __init__(self, x, y, z, nlags = 50, max_dist = 1e4):
        # prepare azimuth/range grid
        X = np.column_stack((x,y))
        self.lags, self.semivariance = self.raw_variogram(X, z)
        self.emp_model = self.fit_variogram()
    
    def raw_variogram(self, X, y, nlags = 50, max_d = 1e4):
        """Return a raw variogram of data        
        input: X = m x n np.array of m points in n dimensional coords
               y = data at points
               nlags = number of bins
               max_d = maximum distance to compute variogram
        """  
        d = pdist(X, metric="euclidean")
        g = 0.5 * pdist(y[:, None], metric="sqeuclidean")
        # remove points with > max_distance:
        g = np.delete(g, d>max_d)
        d = np.delete(d, d>max_d)
        # variogram:
        dmax = np.amax(d)
        dmin = np.amin(d)
        dd = (dmax - dmin) / nlags
        bins = [dmin + n * dd for n in range(nlags)]
        dmax += 0.001
        bins.append(dmax)
        lags = np.zeros(nlags)
        semivariance = np.zeros(nlags)
        for n in range(nlags):
            if d[(d >= bins[n]) & (d < bins[n + 1])].size > 0:
                lags[n] = np.mean(d[(d >= bins[n]) & (d < bins[n + 1])])
                semivariance[n] = np.mean(g[(d >= bins[n]) & (d < bins[n + 1])])
            else:
                lags[n] = np.nan
                semivariance[n] = np.nan
        
        lags = lags[~np.isnan(semivariance)]
        semivariance = semivariance[~np.isnan(semivariance)]
        return lags, semivariance
    
    def gaussian_variogram_model(self, m, d):
        """Gaussian model, m is [psill, range, nugget]"""
        psill = float(m[0])
        range_ = float(m[1])
        nugget = float(m[2])
        return psill * (1.0 - np.exp(-(d ** 2.0) / 
                                     (range_ * 4.0 / 7.0) ** 2.0)) + nugget
    def variogram_residuals(self, params, x, y):
        return self.gaussian_variogram_model(params, x) - y
    
    def fit_variogram(self):
        # initial params:
        x0 = [
            np.amax(self.semivariance) - np.amin(self.semivariance),
            0.25 * np.amax(self.lags),
            np.amin(self.semivariance),
              ]
        bnds = (
            [0.0, 0.0, 0.0],
            [10.0 * np.amax(self.semivariance), np.amax(self.lags), 
             np.amax(self.semivariance)],
               )
        # use 'soft' L1-norm minimization in order to buffer against
        # potential outliers (weird/skewed points)
        res = least_squares(
            self.variogram_residuals,
            x0,
            bounds = bnds,
            loss = "soft_l1",
            args = (self.lags, self.semivariance),
        )
        return res.x
    
    def plot_fit(self):
        fig = plt.figure(figsize=(4.5,3.5),dpi=120)
        ax = fig.add_subplot(111)
        ax.plot(self.lags, self.semivariance, "r*",
                label = 'empirical')
        ax.plot(self.lags,
            self.gaussian_variogram_model(self.emp_model, self.lags),
            "k-", label = 'Gaussian'
        )
        ax.set_title('Semi-variogram', fontsize=15)
        ax.legend(fontsize = 15)
        ax.set_xlabel('d [m]',fontsize=13)


def APS(psc, network, stack, psc0, atmoWindow = 200, 
        plotFlag = False, apsDir = ''):
    """Estimate APS in network of 1st order PSC        
     input: psc - 1st order psc to estimate APS
            network
            psc0 - all psc to interpolate APS
            atmoWindow in [days]
            plotFlag to plot APS spatially in apsDir
    """
    if plotFlag:
        # create APS dir if not existing:
        if not os.path.exists(apsDir):
            os.makedirs(apsDir)
    # parse shapes:
    n_ps, n_ifgs = network['a_ps'].shape
    # phase residuals
    ph_res = network['ps_res'][:]
    # radar coords in [metres]:
    azDist = psc['azimuth'][:]*stack.attrs['azimuthSpacing']
    rDist = psc['range'][:]*stack.attrs['rangeSpacing']
    azDist0 = psc0['azimuth'][:]*stack.attrs['azimuthSpacing']
    rDist0 = psc0['range'][:]*stack.attrs['rangeSpacing']
    n_psc0 = psc0['IFG'].shape[0]
    refIdx = network.attrs['refIdx']
    psc_idx = psc['psc_idx']
    # remove ref.p.
    if ~np.isnan(refIdx):
        azDist = np.delete(azDist, refIdx)
        rDist = np.delete(rDist, refIdx)
        psc_idx = np.delete(psc_idx, refIdx)
    #
    #% 1. temporal filtering of umodelled defo by gaussian window filter:
    Btemp = (network.attrs['dates'] - network.attrs['masterDate'])/365.2425
    atmoWindow = atmoWindow/365.2425
    timeSpan = (np.max(Btemp) - np.min(Btemp)) *1e3
    filterLength = atmoWindow / 3 *1e3 # 3*std
    # Gaussian:
    filt = 1/(np.sqrt(2*np.pi)*0.5*filterLength) * np.exp (
        -0.5*( np.arange(np.ceil(timeSpan)+1)/(0.5*filterLength) )**2)
    # do the filtering:
    ph_filt = np.zeros_like(ph_res)
    for t in np.arange(n_ifgs):
        weights = filt[np.floor(np.abs(Btemp - Btemp[t])*1000).astype('int')+1]
        ph_filt[:,t] = ph_res@weights/np.sum(weights)
    ph_atmo = ph_res - ph_filt
    #
    #% 2. variogram fitting and Kriging interpolation:
    # iterate over interferograms:
    vario = np.zeros((n_ifgs,3))
    #ph_aps = np.zeros_like(ph_atmo)
    ph_aps = np.zeros((n_psc0, n_ifgs))
    for t in np.arange(n_ifgs):
        acqDate = dates.num2date(network.attrs['dates'][t]).strftime('%Y%m%d')
        print('Estimating APS for acqusition '+acqDate)
        # fit variogram:
        q = variogram(azDist, rDist, ph_atmo[:,t])
        vario[t] = q.emp_model
        # avoid nugget~0 (causes singular Kriging system):
        vario[t][2] = max(vario[t][2], vario[t][0]/100)
        # plot fitted variogram:
        #q.plot_fit()
        # perform kriging:
        q = kriging(azDist, rDist, ph_atmo[:,t], vario[t])
        #TODO include ref.p to interpolation:? no
        ph_aps[:,t] = q.interpolate(azDist0, rDist0)
        if plotFlag:
            q.plot(ph_aps[psc_idx, t], outFig = apsDir+acqDate)
    #
    #% plot example APS filtered ts:
    if plotFlag:
        # choose random:
        idx = np.random.randint(0,n_ps,5)
        fig, ax = plt.subplots(5, 1, figsize=(4,10))
        count = 0
        for i in idx:
            ax[count].plot(ph_res[i,:]/-network.attrs['m2ph']*1e3)
            ax[count].plot((ph_res[i,:] - ph_aps[psc_idx[i], :])/-network.attrs['m2ph']*1e3)
            count += 1
        ax[-1].set_xlabel('acquisition')
        plt.suptitle('Example filtered TS [mm]',y=0.91)
    #
    # save to network dataset:
    if 'aps_phase' in network:
        del network['aps_phase']
        del network['vario_model']
    if 'aps_phase' in psc0:
        del psc0['aps_phase']
    network['aps_phase'] = ph_aps[psc_idx,:]
    psc0['aps_phase'] = ph_aps
    network['vario_model'] = vario


def preparePatches(boundingBox, nPatches, overlap = 3):
    # return list of boundingBoxes for parallel proc.
    azSize = boundingBox[0][1]-boundingBox[0][0]+1
    rgSize = boundingBox[1][1]-boundingBox[1][0]+1    
    azPatchSize = np.ceil(azSize/np.sqrt(nPatches))
    rgPatchSize = np.ceil(rgSize/np.sqrt(nPatches))
    minAz = np.arange(boundingBox[0][0], 
                      boundingBox[0][1]-azPatchSize/2, 
                      azPatchSize)
    maxAz = np.append(minAz[1:].copy(), boundingBox[0][1])
    minAz[1:] -= overlap
    minRg = np.arange(boundingBox[1][0], 
                      boundingBox[1][1]-rgPatchSize/2, 
                      rgPatchSize)
    maxRg = np.append(minRg[1:].copy(), boundingBox[1][1])
    minRg[1:] -= overlap
    azInt = np.column_stack((minAz, maxAz)).astype('int')
    rgInt = np.column_stack((minRg, maxRg)).astype('int')
    bboxes = [i for i in itertools.product(map(tuple, azInt),
                                           map(tuple, rgInt))]
    return bboxes
    

def saveHDF(psc, stack, outFile):
    #% hdf dump
    # first change stack datetime to datenum (for hdf encoding):
    stack.masterMetadata['acqDate'] = dates.date2num(stack.masterMetadata['acqDate'])
    for k in stack.metadata.keys():
        stack.metadata[k]['acqDate'] = dates.date2num(stack.metadata[k]['acqDate'])   
    # TODO: also remove None from burst info:    
    # create HDF5 data storage:
    with h5py.File(outFile, "w") as f:
        f.attrs['id'] = stack.id # TODO: check
        # save psc:
        for k,v in psc.items():
            f.create_dataset("psc/"+k, data = v)
        # save stack:
        # save slcs:
        f.create_dataset("stack/slcs", data = np.array(stack.acqDates).astype(int))    
        # save h2ph factors:
        f.create_dataset("stack/h2ph", data = getH2PH(stack))
        f['stack'].attrs['masterDate'] = stack.masterDate
        f['stack'].attrs['orbit'] = stack.masterMetadata['orbit']
        f['stack'].attrs['wavelength'] = stack.masterMetadata['wavelength']
        f['stack'].attrs['rangeSpacing'] = stack.masterMetadata['rangeSpacing']
        f['stack'].attrs['azimuthSpacing'] = stack.masterMetadata['azimuthSpacing']
        f['stack'].attrs['centerLat'] = stack.masterMetadata['centerLat']
        f['stack'].attrs['centerLon'] = stack.masterMetadata['centerLon']
        f['stack'].attrs['centerH'] = stack.masterMetadata['centerH']
        #insarUtils.recursively_save_dict_contents_to_group(f, 'stack/', stack.__dict__)


def openHDF(projfile):
    # TODO: make a copy:       
    return h5py.File(projfile, "a")
    #with h5py.File(projfile, "a") as f:
    #    psc = f['psc']
    #    stack = f['stack']
    #return psc, stack


def HDF2csv(data, outCSV):
    # parse data:
    network = data['network2']
    psc = data['psc2']    
    # prepare header:
    header = ['ID','LAT','LON','HEIGHT','HEIGHT WRT DEM','SIGMA HEIGHT','VEL',
              'SIGMA VEL','SEASONAL','CUMUL.DISP.','COHER','SVET','LVET','IN',
              'FIN','STDEV']
    acqDates = [dates.num2date(t).strftime('%Y%m%d')
                for t in network.attrs['dates']]
    header += acqDates
    # params:
    resH = network['ps_parms'][:,1]
    vel = network['ps_parms'][:,2]*1e3 # [mm/year]
    sig_resH = network['ps_sig_parms'][:,1]
    sig_vel = network['ps_sig_parms'][:,2]*1e3
    coher = network['ps_var'][:]
    std = np.std(network['ps_res'][:], axis=1)/-network.attrs['m2ph']*1e3
    ts = network['ps_displ'][:]/network.attrs['m2ph']*1e3
    # insert refP to est. params:
    refIdx = network.attrs['refIdx']
    if not np.isnan(refIdx):
        resH = np.insert(resH, refIdx, 0)
        vel = np.insert(vel, refIdx, 0)
        sig_resH = np.insert(sig_resH, refIdx, 0)
        sig_vel = np.insert(sig_vel, refIdx, 0)
        coher = np.insert(coher, refIdx, 0)
        std = np.insert(std, refIdx, 0)
        ts = np.insert(ts, refIdx, 0, axis=0)
    # auxiliary:
    cumul_disp = np.sum(ts, axis=1)
    seasonal = np.zeros_like(vel) # TODO: fix
    IN = np.ones_like(vel)
    FIN = IN*len(acqDates)
    # prepare data:
    out = np.column_stack((psc['idx'][:],
                         psc['plh'][:,0]*180/np.pi,
                         psc['plh'][:,1]*180/np.pi,
                         psc['plh'][:,2],
                         resH,
                         sig_resH,
                         vel,
                         sig_vel,
                         seasonal,
                         cumul_disp,
                         coher,
                         psc['range'][:],
                         psc['azimuth'][:],
                         IN,
                         FIN,
                         std,
                         ts
                         ))
    # create pandas dataframe and export to csv
    df = pd.DataFrame(out, columns = header)
    try:
        df.to_csv(outCSV, index=False)
        print('CSV written.')
    except:
        print('Cannot write CSV.')    


def loadCSV_insar(inCSV):
    # read as pandas dataframe:
    ts = pd.read_csv(inCSV,header=0)
    # drop dpplicates in case of sarproz:
    dupl = [col for col in list(ts.columns) if '.1' in col]
    ts = ts.drop(columns = dupl)
    
    insarDates = list(ts.columns[16:])
    try:
        masterIdx = np.where(ts.iloc[0,:][16:] == 0)[0][0]
        masterDate = dates.datestr2num(insarDates[masterIdx])
    except:
        masterIdx = np.nan    
        masterDate = np.nan
    return (ts,insarDates,masterIdx,masterDate)


def prepare_insar(parms):
    # read stack: (for now hardcoded Sentinel-1)
    stack = classes.Stack(parms['stackId'],
                          parms['stackDir'],
                          'Sentinel-1',
                          parms['subswath'],
                          'coreg')
    try:
        stack.readData()
    except:
        print('Perform coregistration first.')
        raise
    # remove dates before startDate:
    stack.reduce(parms['startDate'])
    # start_idx = next(x for x, val in enumerate(stack.acqDates) 
    #                  if val > parms['startDate']) 
    # stack.acqDates = stack.acqDates[start_idx:]
    # if stack.masterDate not in stack.acqDates:
    #     print('Start date specified after master date. Quiting.')
    #     raise
    # stack.files = stack.files[start_idx:]
    # tmp = dict()
    # for k,v in stack.metadata.items():
    #    if k > parms['startDate']:
    #        tmp[k] = v
    # stack.metadata = tmp  
    
    # get AOI bounding box:
    boundingBox = radarUtils.getAOIbox(stack, 
                                       parms['min_lon'], 
                                       parms['max_lon'], 
                                       parms['min_lat'], 
                                       parms['max_lat'],
                                       parms['aver_h'])
    # TODO: decide on number of patches for efficient reading
    # select PSc:
    psc = selectPS(stack, boundingBox, parms['D_A_thr'])    
    # save to HDF5 dump:
    if not os.path.exists(parms['outDir']):
        os.makedirs(parms['outDir'])
    HDFfile = parms['outDir'] + os.sep + 'insar_' + parms['stackId'] + '.hdf5'
    saveHDF(psc, stack, HDFfile)
    #% open hdf5 
    data = openHDF(HDFfile)
    return data


def snap2HDF(parms, outHDF = ''):
    '''
    Parameters
    ----------
    parms : dict containing insar.parms
    outHDF : string
        Full path to output HDF.

    Returns
    -------
    stack object

    '''
    # prepare output:
    if not outHDF:
        outHDF = (parms['outDir'] + os.sep + 'stack_' 
                  + parms['stackId'] + '.hdf5')
    # initialise stack:
    stack = classes.Stack(parms['stackId'],
                      parms['stackDir'],
                      'Sentinel-1',
                      parms['subswath'],
                      'coreg')
    stack.readData()
    # remove dates before startDate:
    print('Reducing to specified startDate...')
    stack.reduce(parms['startDate'])
    # prepare boundingBox:
    boundingBox = radarUtils.getAOIbox(stack, 
                                       parms['min_lon'], 
                                       parms['max_lon'], 
                                       parms['min_lat'], 
                                       parms['max_lat'],
                                       parms['aver_h'])

    # load master and find usable boundingBox:
    master_file = stack.files[stack.masterIdx]
    master_SLC = s1Utils.readSLC(master_file, boundingBox, method = 'coreg',
                                  deramp = False)
    first_valid_range = np.where(master_SLC[0,:] != 0j)[0][0]
    if first_valid_range > 0:
        print('Bounding box modified for first valid range sample: ' 
              + str(first_valid_range))
        # fix boundingBox:
        boundingBox = (boundingBox[0], 
                       (boundingBox[1][0] + first_valid_range, 
                        boundingBox[1][1]))
    # read master SLC again:
    master_SLC = s1Utils.readSLC(master_file, boundingBox, method = 'coreg',
                                      deramp = False)
    # read derampPhase:
    derampPhase = s1Utils.readDerampPhase(master_file, boundingBox)
    # read HGT:
    hGrid = s1Utils.readHGT(master_file,boundingBox)
    # prepare size:
    azSize = boundingBox[0][1]-boundingBox[0][0]+1
    rgSize = boundingBox[1][1]-boundingBox[1][0]+1
    # create HDF:
    bad_idx = [] # store corrupt SLC
    with h5py.File(outHDF, "w") as f:
        # loop through SLCs:
        idx = 0
        SLC = np.zeros((azSize, rgSize), dtype='cfloat')
        IFG = np.zeros((azSize, rgSize), dtype='cfloat')
        for file,acqDate in zip(stack.files, stack.acqDates):
            metadata = stack.metadata[acqDate]
            SLC = s1Utils.readSLC(file, boundingBox, method = 'coreg',
                                  deramp = False)
            # check for holes:
            if np.count_nonzero(SLC==0j)/SLC.size > 0.01: # allow 1%
                print('Image '+ acqDate + 
                      ' contains many zero values! skipping.')
                bad_idx.append(idx)
            else:
                #
                IFG = s1Utils.readIFG(file, boundingBox)
                f.create_dataset("SLC/"+acqDate, data = SLC)
                f['SLC/'+acqDate].attrs['beta0'] = metadata['beta0']
                f.create_dataset("IFG/"+acqDate, data = IFG)
            idx += 1
            print('Reading SLC: ',idx,'/',len(stack.files))
        # save master SLC:
        f.create_dataset('master_SLC', data = master_SLC)
        f['master_SLC'].attrs['master_date'] = stack.masterDate
        f['master_SLC'].attrs['boundingBox'] = boundingBox
        # save master derampPhase:
        f.create_dataset("derampPhase", data = derampPhase)
        # save height:
        f.create_dataset('HGT', data = hGrid)
    # remove bad_idx SLC from stack:
    if bad_idx:
        stack.remove_idx(bad_idx)
    # store stack:
    ioUtils.toJSON(stack, parms['outDir'])
    # return stack and log
    print('HDF prepared for InSAR processing.')
    return stack, outHDF


def selectPSc(stack, stackHDF, D_A_thr = 0.25, plotFlag = 1, ovs_factor = 1):
    '''
    Parameters
    ----------
    stack : Stack
    stackHDF : string
        full path to HDF containing stack (see 'snap2HDF').
    D_A_thr : float
        Normalised amplitude dispersion threshold.

    Returns
    -------
    psc : dict
        Containing all PSc data.
    '''
    d = openHDF(stackHDF)
    boundingBox = d['master_SLC'].attrs['boundingBox']
    azSize, rgSize = d['master_SLC'].shape
    # allocate:    
    beta0array = np.zeros((azSize, rgSize, stack.nSLC))
    idx = 0
    # prepare beta0 time series:
    for k in d['SLC'].keys():
        beta0array[:,:,idx] = (np.power(np.abs(d['SLC'][k][:]),2)/
                               (d['SLC'][k].attrs['beta0']**2))
        idx += 1
    # temporal average beta0 in dB scale (closer to normal distrib.):    
    meanBeta_dB = np.nanmean(10*np.log10(beta0array+1e-14), axis=2)
    meanAmp = np.sqrt(np.power(10, (meanBeta_dB/10)))
    stdAmp = np.nanstd(np.sqrt(beta0array), axis=2)
    del beta0array
    D_A = stdAmp/meanAmp
    # plot:
    # if plotFlag:
    #     f, ax = plt.subplots(figsize=(8,8),dpi=200)
    #     im = ax.imshow(meanBeta_dB, cmap='gray', aspect=5)
    #     im.set_clim(-20,10)
    #     plt.colorbar(im)
    #% remove sidelobes:
    peaks = peak_local_max(meanAmp)
    azIdx = peaks.T[0]
    rgIdx = peaks.T[1]
    # use D_A threshold:
    peak_D_A = D_A[peaks.T[0],peaks.T[1]]
    azIdx = azIdx[peak_D_A < D_A_thr]
    rgIdx = rgIdx[peak_D_A < D_A_thr]
    #read SLC and IFG:
    SLCarray = np.zeros((azIdx.size, stack.nSLC), dtype='cfloat')
    IFGarray = np.zeros((azIdx.size, stack.nSLC), dtype='cfloat')
    idx = 0
    for k in d['SLC'].keys():
        SLC = d['SLC'][k][:]
        SLCarray[:,idx] = SLC[azIdx, rgIdx]
        del SLC
        IFG = d['IFG'][k][:]
        IFGarray[:,idx] = IFG[azIdx, rgIdx]
        del IFG
        idx += 1
    # get sub-pixel positions
    if ovs_factor > 1:
        print('Estimating sub-pixel positions of PSc...')
        crop_factor = 2
        # prepare ovs. master
        SLCderamp = s1Utils.deramp(d['master_SLC'][:], d['derampPhase'])
        
        azCorr = np.zeros_like(azIdx, dtype='float')
        rgCorr = np.zeros_like(azIdx, dtype='float')
        # remove border points:
        mxAz, mxRg = meanAmp.shape
        borderIdx = np.where((rgIdx < crop_factor) | (azIdx < crop_factor)
                             | (azIdx > mxAz-crop_factor) | (rgIdx > mxRg-crop_factor))[0]
        # estimate sub-pix pos
        for i in range(len(azIdx)):
            if i in borderIdx:
                continue
            SLCcrop = SLCderamp[azIdx[i]-crop_factor:azIdx[i]+crop_factor+1, 
                                rgIdx[i]-crop_factor:rgIdx[i]+crop_factor+1]
            AMPovs = np.abs(radarUtils.oversample(SLCcrop, ovs_factor))
            # get central sub-crop:
            minCrop = round(AMPovs.shape[0]/2 -ovs_factor*1.5)
            maxCrop = round(AMPovs.shape[0]/2 + ovs_factor*0.5)
            AMPovs_crop = AMPovs[ minCrop:maxCrop, minCrop:maxCrop]  
            # max method
            idx = np.unravel_index(np.argmax(AMPovs_crop, axis=None), 
                                   AMPovs_crop.shape)
            # TODO: estimate peak
            azCorr[i] = (minCrop + idx[0])/ovs_factor - crop_factor
            rgCorr[i] = (minCrop + idx[1])/ovs_factor - crop_factor
        # correct az/rg:
        az = boundingBox[0][0] + azIdx + azCorr
        rg = boundingBox[1][0] + rgIdx + rgCorr
    else:
        az = boundingBox[0][0] + azIdx
        rg = boundingBox[1][0] + rgIdx
    # prepare psc dict:
    psc = {'azIdx': azIdx,
           'rgIdx': rgIdx,
           'azimuth': az,
           'range': rg,
           'SLC': SLCarray,
           'IFG': IFGarray,
           'D_A': D_A[azIdx,rgIdx]
           }
    hGrid = d['HGT'][:]
    h = hGrid[azIdx,rgIdx]
    del hGrid
    # geocode:
    psc['xyz'],psc['satvec'] = radarUtils.radar2xyz(
        psc['azimuth'], psc['range'], h, stack.masterMetadata, 
        returnSat=True)
    psc['plh'] = geoUtils.xyz2plh(psc['xyz'])
    # incidence:
    psc['incAngle'] = geoUtils.xyz2zas(psc['xyz'], psc['satvec'])[:,0]
    # close HDF:
    if 'meanBeta' not in d:
        d.create_dataset('meanBeta', data = meanBeta_dB)
    if plotFlag:
        plotUtils.plot_mean_beta0(d, outDir = os.path.split(stackHDF)[0])
    d.close()
    # log:
    print(str(azIdx.size) + ' PSc selected.')
    return psc


def order_psc(data, thr = 0.25):
    idx = data['psc']['D_A'][:] < thr
    idx1 = np.where(idx)[0]
    idx2 = np.where(~idx)[0]
    for k, v in data['psc'].items():
        data.create_dataset('psc_A/'+k, data=v[idx1])
    for k, v in data['psc'].items():
        data.create_dataset('psc_B/'+k, data=v[idx2])
    del data['psc']
    data.move('psc_A/','psc/')


def prepare_insar_HDF(parms):
    # prepare dir:
    if not os.path.exists(parms['outDir']):
        os.makedirs(parms['outDir'])
    # load stack:
    stackHDF = (parms['outDir'] + os.sep + 'stack_' 
                + parms['stackId'] + '.hdf5')
    if not os.path.isfile(stackHDF):
        stack, stackHDF = snap2HDF(parms)
    else:
        stackJSON = stackHDF.replace('hdf5','json')
        stack = ioUtils.fromJSON(stackJSON)
    # select PSc:
    psc = selectPSc(stack, stackHDF, D_A_thr = parms['D_A_thr'], 
                    plotFlag = parms['plot_flag'])
    # save to HDF5 dump:
    HDFfile = parms['outDir'] + os.sep + 'insar_' + parms['stackId'] + '.hdf5'
    saveHDF(psc, stack, HDFfile)
    #% open hdf5 
    data = openHDF(HDFfile)
    return data


def selectPS(stack, boundingBox, D_A_thr):
    # allocate time series array
    azSize = boundingBox[0][1]-boundingBox[0][0]+1
    rgSize = boundingBox[1][1]-boundingBox[1][0]+1
    beta0array = np.zeros((azSize, rgSize, stack.nSLC))                                  
    slc_array = np.zeros_like(beta0array, dtype='cfloat')
    ifg_array = np.zeros_like(beta0array, dtype='cfloat')
    # loop through SLCs:
    idx = 0
    for file,acqDate in zip(stack.files, stack.acqDates):
        metadata = stack.metadata[acqDate]
        SLC = s1Utils.readSLC(file, boundingBox, method = 'coreg', deramp = False)
        slc_array[:,:,idx] = SLC
        beta0array[:,:,idx] = np.power(np.abs(SLC), 2)/(metadata['beta0']**2)
        # load ifgs:
        ifg_array[:,:,idx] = s1Utils.readIFG(file, boundingBox)
        idx += 1
        print('Reading SLC: ',idx,'/',len(stack.files))
    # temporal average beta0 in dB scale (closer to normal distrib.):
    meanBeta_dB = np.nanmean(10*np.log10(beta0array+1e-14), axis=2)
    meanAmp = np.sqrt(np.power(10, (meanBeta_dB/10)))
    stdAmp = np.nanstd(np.sqrt(beta0array), axis=2)
    del beta0array
    # D_A:
    D_A = stdAmp/meanAmp
    #% only find peaks:
    peaks = peak_local_max(meanAmp)
    azIdx = peaks.T[0]
    rgIdx = peaks.T[1]
    peak_D_A = D_A[peaks.T[0],peaks.T[1]]
    azIdx = azIdx[peak_D_A < D_A_thr]
    rgIdx = rgIdx[peak_D_A < D_A_thr]
    #load DEM heights:
    hGrid = s1Utils.readHGT(file,boundingBox)
    # prepare psc dict:
    psc = {'azIdx': azIdx,
           'rgIdx': rgIdx,
           'azimuth': boundingBox[0][0] + azIdx,
           'range': boundingBox[1][0] + rgIdx,
           'SLC': slc_array[azIdx,rgIdx],
           'IFG': ifg_array[azIdx,rgIdx],
           'D_A': D_A[azIdx,rgIdx]
           #'h': hGrid[azIdx,rgIdx]
           }
    h = hGrid[azIdx,rgIdx]
    # geocode:
    psc['xyz'],psc['satvec'] = radarUtils.radar2xyz(
        psc['azimuth'], psc['range'], h, stack.masterMetadata, 
        returnSat=True)
    psc['plh'] = geoUtils.xyz2plh(psc['xyz'])
    # incidence:
    psc['incAngle'] = geoUtils.xyz2zas(psc['xyz'], psc['satvec'])[:,0]
    # loop and compute h2ph factors:
    # idx = 0
    # meanBperp = np.zeros(stack.nSLC)
    # h2ph_array = np.zeros_like(psc['SLC'], dtype=np.float)
    # for slave in stack.metadata.keys():
    #     metadata = stack.metadata[slave]
    #     _,_,satvec = radarUtils.xyz2t(psc['xyz'],metadata)
    #     # baseline:
    #     B = np.linalg.norm(psc['satvec'] - satvec, axis=1)
    #     # paralel baseline (master distance - slave distance):
    #     distM = np.linalg.norm(psc['satvec'] - psc['xyz'], axis=1)
    #     B_par =  distM - np.linalg.norm(satvec - psc['xyz'], axis=1)
    #     incAngleSlv = geoUtils.xyz2zas(satvec,psc['xyz'])[:,0]    
    #     if np.mean(psc['incAngle']) > np.mean(incAngleSlv):
    #         B_perp = np.sqrt(B**2 - B_par**2)
    #     else:
    #         B_perp = -np.sqrt(B**2 - B_par**2)
    #     # h2ph factor:
    #     h2ph_array[:,idx] = B_perp/(distM*np.sin(psc['incAngle']))
    #     meanBperp[idx] = np.mean(B_perp)
    #     idx += 1
    # psc['h2ph'] = h2ph_array
    return psc


def createNetwork(psc, stack, n_type = 'redundant', n_con = 8, max_dist = 5e3, 
                  plotFlag = 1):
    
    # parse data:
    azDist = psc['azimuth']*stack.attrs['azimuthSpacing']
    rDist = psc['range']*stack.attrs['rangeSpacing']
    n_psc = azDist.size
    # points array:        
    X = np.column_stack((azDist, rDist))
    # network types:
    if n_type == 'delaunay':
        tr = tri.Triangulation(azDist, rDist)
        arcs = tr.edges
        # if plotFlag:
        #     f, ax = plt.subplots()
        #     ax.triplot(tr)
        #     ax.set_title('Estimation network')
        #     ax.set_xlabel('azimuth [pix]')
        #     ax.set_ylabel('range [pix]')
            
        # TODO: filter by maxArcLength
    elif n_type == 'redundant':
        arcs = np.empty((n_psc*n_con,2))
        arcs[:] = np.nan
        # pair-wise distance matrix:
        dist = cdist(X, X)
        # pair-wise azimuth-angle matrix:
        angl = cdist(X, X, lambda u, v: 
                     np.arctan2(u[0]-v[0], u[1]-v[1])*180/np.pi + 180)
        # remove diagonal (points themselves)
        np.fill_diagonal(angl, np.nan)
        np.fill_diagonal(dist, np.nan)
        # sort to quadrants:
        quad = np.digitize(angl, np.arange(0, 361, 360/n_con))    
        # loop quadrants and take closest:
        neigh = np.empty((n_con, n_psc), dtype = int)
        for q in np.arange(n_con):
            # mask out non-quadrant points:
            mask = np.ma.masked_array(dist, quad != q+1)
            # mask out points > max distance:
            #mask = np.ma.masked_array(mask, dist > max_dist)
            # get closest neighbours indices:
            neigh[q] = np.nanargmin(mask, axis=1) 
        # make arc's array:
        arcs = np.repeat(np.arange(n_psc), n_con)
        arcs = np.column_stack(( arcs, neigh.T.flatten() ))
        # remove non-unique:
        #arcs = np.unique(arcs, axis=0)
        #
    else:
        print('Unknown network type.')
    # remove non-unique arcs:
    arcs = np.unique(arcs, axis=0)
    # remove p-p arcs:
    arcs = np.delete(arcs, np.where(arcs[:,0] == arcs[:,1]), axis = 0)
    # remove arcs > max_dist:
    dst = np.linalg.norm(X[arcs[:,0]] - X[arcs[:,1]], axis=1)
    arcs = np.delete(arcs, np.where(dst > max_dist), axis = 0)
    #
    # plot:
    if plotFlag:
        f, ax = plt.subplots(figsize=(8,8),dpi=120)
        for (i, j) in arcs:
            ax.plot([rDist[i], rDist[j]],
                    [azDist[i], azDist[j]],
                    "-r", lw = 0.5)
        ax.set_title('Estimation network')
        ax.set_xlabel('range [m]')
        ax.set_ylabel('azimuth [m]')
        ax.invert_yaxis()
    #network = {
    #    'arcs': arcs}
    #return network
    return arcs


def functionalModel(stack, model='linear'):
    # m2ph factors:
    m2ph = -4*np.pi/stack.attrs['wavelength']      
    # time differences:
    dat = dates.datestr2num(stack['slcs'][:].astype('str'))
    master = dates.datestr2num(stack.attrs['masterDate'])
    dt = (dat - master)/365.2425
    # get hgt conversion from perp. baselines:
    h2ph = stack['h2ph'][:]
    # factors:
    alpha = m2ph*dt
    beta = m2ph*h2ph
    gamma1 = -m2ph*np.sin(2*np.pi*dt)
    gamma2 = m2ph*(np.cos(2*np.pi*dt)-1)
    # design matrix:
    if model == 'linear':
        A = np.array([np.ones(len(alpha)), beta, alpha]).T
    elif model == 'seasonal':
        A = np.array([np.ones(len(alpha)), beta, alpha,
                      gamma1, gamma2]).T
    else:
        print('Unknown model')
        raise
    return A


def getH2PH(stack):
    # get hgt conversion from perp. baselines:
    plh = np.array([stack.masterMetadata['centerLat']/180*np.pi,
                    stack.masterMetadata['centerLon']/180*np.pi,
                    stack.masterMetadata['centerH']])
    xyz = geoUtils.plh2xyz(plh)
    _,_,masterOrbit = radarUtils.xyz2t(xyz,stack.masterMetadata) 
    incMaster,_,_ = geoUtils.xyz2zas(xyz,masterOrbit)
    distMaster = np.linalg.norm(masterOrbit-xyz)
    idx = 0
    B_perp = np.zeros(len(stack.acqDates))
    for acqDate in stack.acqDates:
        metadata = stack.metadata[acqDate]
        _,_,slaveOrbit = radarUtils.xyz2t(xyz,metadata) 
        B = np.linalg.norm(slaveOrbit[0] - masterOrbit[0])
        incSlave,_,_ = geoUtils.xyz2zas(xyz,slaveOrbit)
        distSlave = np.linalg.norm(slaveOrbit-xyz)
        B_par = distMaster - distSlave
        if incMaster > incSlave:
            B_perp[idx] = np.sqrt(B**2-B_par**2)
        else:
            B_perp[idx] = -np.sqrt(B**2-B_par**2)
        idx += 1
    h2ph = B_perp/(distMaster*np.sin(incMaster))
    return h2ph

def getPreciseH2PH(psc, stack):
    if 'h2ph' not in psc:
        xyz = psc['xyz'][:]
        master_satvec = psc['satvec'][:]
        incAngle_master = psc['incAngle'][:]
        # loop and compute h2ph factors:
        idx = 0
        # meanBperp = np.zeros(stack.nSLC)
        h2ph_array = np.zeros_like(psc['SLC'], dtype=np.float)
        for slave in stack.metadata.keys():
             metadata = stack.metadata[slave]
             _,_,satvec = radarUtils.xyz2t(xyz, metadata)
             # baseline:
             B = np.linalg.norm(master_satvec - satvec, axis=1)
             # paralel baseline (master distance - slave distance):
             distM = np.linalg.norm(master_satvec - xyz, axis=1)
             B_par =  distM - np.linalg.norm(satvec - xyz, axis=1)
             incAngleSlv = geoUtils.xyz2zas(satvec, xyz)[:,0]    
             if np.mean(incAngle_master) > np.mean(incAngleSlv):
                 B_perp = np.sqrt(B**2 - B_par**2)
             else:
                 B_perp = -np.sqrt(B**2 - B_par**2)
             # h2ph factor:
             h2ph_array[:,idx] = B_perp/(distM*np.sin(incAngle_master))
        #     meanBperp[idx] = np.mean(B_perp)
             idx += 1
        psc['h2ph'] = h2ph_array
    

# def arcSolve(y, Qy, B1, model='linear', method='bootstrap' ,corr=False):
#     # construct design matrix including pseudoobs.
#     nMeas,nUnknowns = B1.shape    
#     A1 = -2*np.pi*np.eye(nMeas)
#     #A2 = np.zeros((nUnknowns,nMeas))
#     #B2 = np.eye(nUnknowns);
#     #A = np.vstack((np.hstack((A1,B1)),
#     #               np.hstack((A2,B2))))
#     # pseudo-observations == number of unknowns
#     #b0 = np.zeros((nUnknowns,1))    
#     # cov. matrix of pseudo-observations
#     Q_b0 = pseudoVCM(model)
#     # float solution:
#     a = y/(2*np.pi)
#     # cov matrix:
#     Q_a = 1/(4*np.pi**2)*(Qy + B1@Q_b0@B1.T)
#     Q_a = np.tril(Q_a)+np.tril(Q_a,-1).T
#     # LAMBDA:
#     #if method == 'bootstrap':
#     a_ILS = LAMBDA.main(a,Q_a)[0][:,0]
#     # fixed-solution
#     Qy_inv = np.diag(np.diag(Qy))
#     N = (B1.T@Qy_inv@B1) # normal equations
#     N_inv = np.linalg.inv(N)
#     x_fixed = N_inv@(B1.T)@Qy_inv@(y+A1@a_ILS)
#     sig_x_fixed = np.sqrt(np.diag(N_inv))
#     uw_ph = (y+A1@a_ILS)
#     model_ph = B1@x_fixed
#     # if res.h. correction required:
#     if corr:
#         resHph = B1[:,1]*x_fixed[1]
#         uw_ph -= resHph
#         model_ph -= resHph
#     return uw_ph,x_fixed,sig_x_fixed,model_ph


def arcSolveFast(y, Z, L, iZt, N_i, B1, Qy_i):
    # float solution:
    a = y/(2*np.pi)
    # lambda bootstrap solution:
    a = lambda_fast(a, Z, L, iZt)
    # unwrapped solution
    uw_ph = y - a*2*np.pi
    x = N_i@(B1.T)@Qy_i@uw_ph
    res = uw_ph - B1@x
    var = (res.T@Qy_i@res)/(B1.shape[0]-B1.shape[1])
    return a, uw_ph, x, res, var


def lambdaPrepare(Qy, B1, model='linear'):
    # cov. matrix of pseudo-observations
    Q_b0 = pseudoVCM(model)
    # cov matrix of ambiguities:
    Q_a = 1/(4*np.pi**2)*(Qy + B1@Q_b0@B1.T)
    Q_a = np.tril(Q_a)+np.tril(Q_a,-1).T
    # decorrelate:
    Z,L,D,iZt = lambda_decorrel(Q_a)
    return Z, L, iZt


def ldldecom(Qahat):
    """ 
    # ----------------------------------------------------------------------- #
    #   ldldecom: Find LtDL-decomposition of Qahat-matrix                     #
    #                                                                         #
    #             L,D = ldldecom(Qahat)                                       #
    #                                                                         #             
    #   This function finds the LtDL decomposition of a given ambiguity       #
    #   variance-covariance matrix.                                           #
    #                                                                         #
    #   Input arguments:                                                      #
    #       Qahat: Variance-covariance matrix of ambiguities to be factored   #
    #              (symmetric n-by-n matrix; 2D numpy array)                  #
    #                                                                         #
    #   Output arguments:                                                     #
    #       L    : n-by-n factor matrix (2D numpy array)                      #
    #              (unit lower triangular)                                    #
    #       D    : Diagonal n-vector (1D numpy array)                         #
    #                                                                         #
    # ----------------------------------------------------------------------- #
    #   Function : ldldecom                                                   #
    #   Date     : 19-MAY-1999                                                #
    #   Author   : Peter Joosten                                              #
    #              Mathematical Geodesy and Positioning,                      #
    #              Delft University of Technology                             #
    # ----------------------------------------------------------------------- #
    """ 
    
    # Find the total number of ambiguities
    n = len(Qahat)
    
    # Initialize the n-by-n lower triangular matrix and the diagonal n-vector
    L    = np.zeros((n,n))
    D    = np.empty((1,n))
    D[:] = np.nan

    # Decorrelate the input vc-matrix starting from its last row
    for i in range(n-1,-1,-1):
    
        D[0,i] = Qahat[i,i].copy()
        
        L[i,0:i+1] = Qahat[i,0:i+1]/np.sqrt(Qahat[i,i])
    
        for j in range(0,i,1):
            Qahat[j,0:j+1] -= L[i,0:j+1]*L[i,j]
        
        L[i,0:i+1] /= L[i,i]
        
    # Terminate in case the input vc-matrix is not positive definite      
    if np.any((D<1e-10)) == True:
        print("Error: the input vc-matrix (in function ldldecom) is not " +  
               "positive definite!")
        raise SystemExit
        
    return L, D


def lambda_decorrel(Qa):
    """ 
    # ----------------------------------------------------------------------- #
    #   decorrel: Decorrelate a (co)variance matrix of ambiguities            #
    #                                                                         #
    #             Qzhat,Z,L,D,zhat,iZt = decorrel(Qahat)                 #
    #                                                                         #             
    #   This function creates a decorrelated Q-matrix, by finding the         #
    #   Z-matrix and performing the corresponding transformation.             #
    #                                                                         #                                                                  #
    #   Input arguments:                                                      #
    #       Qahat: Variance-covariance matrix of ambiguities (original)       #
    #              (symmetric n-by-n matrix; 2D numpy array)                  #
    #                                                                         #
    #   Output arguments:                                                     #
    #       Qzhat: Variance-covariance matrix of decorrelated ambiguities     #
    #       Z    : Z-transformation matrix                                    #
    #       L    : L matrix (from LtDL-decomposition of Qzhat)                #
    #       D    : D matrix (from LtDL-decomposition of Qzhat)                #                                  #
    #       iZt  : inv(Z')-transformation matrix                              #
    #                                                                         #
    # ----------------------------------------------------------------------- #
    #   Function : decorrel                                                   #
    #   Date     : 19-MAY-1999 / modified 12-APR-2012                         #
    #   Author   : Peter Joosten / Sandra Verhagen                            #
    #              Mathematical Geodesy and Positioning,                      #
    #              Delft University of Technology                             #
    # ----------------------------------------------------------------------- #
    """                                                                             
    # Get the eigenvalues (D) and eigenvectors (V) of Qahat
    D,V = np.linalg.eig(Qa) 
    
    # Initialization
    n   = len(Qa)
    iZt = np.eye(n)
    i1  = n-2
    sw  = 1
    
    # LtDL decomposition
    L,D = ldldecom(Qa.copy())
    
    # Decorrelation procedure
    while sw:
        i  = n-1 # loop for column from n-1 to 0
        sw = 0    
        while (not sw) and (i > 0):   
            i-=1 # the i-th column
            if i <= i1:
                for j in range(i+1,n):    
                    mu = np.round(L[j,i])
                    if mu != 0.0: # if mu not equal to 0
                        L[j:n,i] -= mu*L[j:n,j]
                        iZt[:,j] += mu*iZt[:,i] # iZt is inv(Zt) matrix
            delta = D[0,i] + L[i+1,i]**2 * D[0,i+1]
            if delta < D[0,i+1]:  
                lamda    = D[0,i+1] * L[i+1,i] / delta
                eta      = D[0,i] / delta
                D[0,i]   = eta*D[0,i+1]
                D[0,i+1] = delta            
                L[i:i+2,0:i] = \
                     np.array([[-L[i+1,i],1],[eta, lamda]]).dot(L[i:i+2,0:i])
                L[i+1,i] = lamda      
                # Swap rows i and i+1            
                if i==0:           
                    L[i+2:n,i:i+2:1] = L[i+2:n,i+1::-1].copy()
                    iZt[:,i:i+2:1]   = iZt[:,i+1::-1].copy()                
                else:               
                    L[i+2:n,i:i+2:1] = L[i+2:n,i+1:i-1:-1].copy()
                    iZt[:,i:i+2:1]   = iZt[:,i+1:i-1:-1].copy()    
                i1 = i
                sw = 1    
    # Return the transformed Q-matrix and the transformation matrix     
    Z     = np.round(np.linalg.inv(iZt.T))
    #Qzhat = Z.T.dot(Qa).dot(Z)
    return Z,L,D,iZt


def bootstrap(ahat,L):
    """ 
    # ----------------------------------------------------------------------- #
    #   bootstrap: Bootstrapping for integer ambiguity resolution             #
    #                                                                         #
    #              afixed = bootstrap(ahat,L)                                 #
    #                                                                         #
    #   This function performs the boostrapping technique for integer         #
    #   ambiguity resolution.                                                 #
    #                                                                         #
    #   Input arguments:                                                      #
    #       ahat : float ambiguities (1D numpy array)                         #
    #       L    : unit lower triangular matrix from LtDL-decomposition of    #
    #              the vc-matrix                                              #
    #                                                                         #        
    #   Output arguments:                                                     #
    #      afixed: fixed ambiguities (1D numpy array)                         #
    #                                                                         #        
    #   Note:                                                                 #
    #       The success rate with boostrapping depends on the parametrization #
    #       of the float ambiguities ahat. It should be applied to the        #
    #       decorrelated ambiguities. The complete procedure is then:         #
    #           Qzhat,Z,L,D,zhat,iZt = decorrel(Qahat,ahat)                   # 
    #           zfixed = bootstrap(zhat,L)                                    #
    #           afixed = iZt.dot(zfixed)                                      #
    #                                                                         #        
    # ----------------------------------------------------------------------- #
    #   Function : boostrap                                                   #
    #   Date     : 06-SEP-2010                                                #
    #   Author   : Bofeng Li                                                  #
    #              GNSS Research Center, Department of Spatial Sciences,      #
    #              Curtin University of Technology                            #
    # ----------------------------------------------------------------------- #
    """   
                                                                            
    # Number of ambiguities
    n = len(ahat)

    # Initialize vectors to store conditional and fixed ambiguities
    afcond = np.zeros(n,)
    afixed = np.zeros(n,)

    # Initialize vector to compute conditional a
    S = np.zeros(n,)

    # Start from last ambiguity (should preferably be the most precise one)
    afcond[n-1] = ahat[n-1]

    # Rounding of last ambiguity
    afixed[n-1] = round(afcond[n-1])

    for i in range(n-2,-1,-1):
    
        # Compute the i-th cond. ambiguity on the ambiguities from i+1 to n
        S[0:i+1] = S[0:i+1] + (afixed[i+1]-afcond[i+1])*L[i+1,0:i+1]
    
        afcond[i] = ahat[i] + S[i]
        afixed[i] = round(afcond[i])                                                                            
        
    return afixed


def pseudoVCM(model, bounds=bounds):
    # cov. matrix of pseudo-observations
    # significance factor
    k = 2.5
    sig_Matmo = 30/180*np.pi;           #in radians ~ 10 mm = 60deg
    sig_dH = bounds['dH'] /k;           # [m]
    sig_D = bounds['vel']/1e3/k;        # [m]
    sig_DS = bounds['seasonal']/1e3/k  # [m]
    # construct VCM based on model unknowns:
    if model == 'linear':
        Q_b0 = np.diag(np.array([sig_Matmo, sig_dH, sig_D])**2)
    elif model == 'seasonal':
        Q_b0 = np.diag(np.array([sig_Matmo, sig_dH, sig_D, 
                                 sig_DS, sig_DS])**2)
    return Q_b0


def lambda_fast(a, Z, L, iZt):
    # solve ambis. by bootsrap:
    zhat  = Z.T.dot(a)
    zfixed = bootstrap(zhat,L)
    afixed = iZt.dot(zfixed)
    return afixed


def VCE(VC, B1, y):
    # variance component estimation for DD InSAR phase time-series
    no_ifg = len(VC)
    Q_s = np.zeros((no_ifg, no_ifg)) # initialize cov.matrix
    #I = eye(size(y,1)); % identity matrix
    #P_A = I - A*((A'/Q_y*A)\A'/Q_y); # orthogonal projector P_A
    #e = P_A@y  # vector of least-squares residuals
    r = np.zeros(no_ifg)
    Qy = np.diag(VC)
    Qy_i = np.diag(1/np.diag(Qy)) # inverse of diagonal matrix
    # orthogonal projector:
    P_A = np.eye(no_ifg) - B1@(np.linalg.inv(B1.T@Qy_i@B1)@B1.T@Qy_i)
    res = P_A@y # vector of least-squares residuals
    Q_P_A = Qy_i@P_A
    for i in np.arange(no_ifg):
        Q_v = Q_s.copy()
        Q_v[i,i] = 2; # 2, no 1 -- see derivation in Freek phd
        r[i] = 0.5*(res.T@Qy_i@Q_v@Qy_i@res)
    N = 2*(Q_P_A*Q_P_A.T)
    VC = np.linalg.inv(N)@r
    VC[VC < 0] = (10/180*np.pi)**2 # minimum variance factor
    # 2nd iteration:
    Qy = np.diag(VC)
    Qy_i = np.diag(1/np.diag(Qy)) # inverse of diagonal matrix
    # orthogonal projector:
    P_A = np.eye(no_ifg) - B1@(np.linalg.inv(B1.T@Qy_i@B1)@B1.T@Qy_i)
    res = P_A@y # vector of least-squares residuals
    Q_P_A = Qy_i@P_A
    for i in np.arange(no_ifg):
        Q_v = Q_s.copy()
        Q_v[i,i] = 2; # 2, no 1 -- see derivation in Freek phd
        r[i] = 0.5*(res.T@Qy_i@Q_v@Qy_i@res)
    N = 2*(Q_P_A*Q_P_A.T)
    VC = np.linalg.inv(N)@r
    VC[VC < 0] = (10/180*np.pi)**2 # minimum variance factor    
    return VC


def ref_coords2idx(psc0, refAz, refR):
    return np.intersect1d(np.nonzero(psc0['azimuth'][:] == refAz),
                         np.nonzero(psc0['range'][:] == refR))[0]


def temporalAmbiguity(stack, psc, network, model='seasonal', plotFlag=0):
    
    # parse data:
    #psc = data['psc']
    #network = data['network']
    #stack = data['stack']
    #
    n_ps, n_ifg = psc['SLC'].shape
    arcs = network['arcs'][:]
    m2ph = -4*np.pi/stack.attrs['wavelength']
    # prepare phases per arcs:
    dPhase = np.angle(psc['IFG'])
    # fix APS if estimated:
    if 'aps_phase' in psc:
        aps_phase = psc['aps_phase'][:]
        if 'refIdx' in network.attrs:
            if ~np.isnan(network.attrs['refIdx']):
                refIdx0 = ref_coords2idx(psc, network.attrs['refAz'], 
                                         network.attrs['refR']) 
                # intesrt ref.idx zeros:
                #np.insert(aps_phase, network.attrs['refIdx'], 0, axis=0)
                # null refIdx aps:
                aps_phase[refIdx0, :] = 0
                # TODO: check
        # correct dPhases:
        dPhase -= aps_phase

    arcPhase = dPhase[arcs[:,1],:] - dPhase[arcs[:,0],:] # not re-wrapped
    # prepare functional model:
    B1 = functionalModel(stack, model = model)
    # prepare apriori-VCM:
    if 'VC_mean' in network:
        VC = network['VC_mean'][:]
    else:
        sig0 = np.sqrt(2*np.mean(psc['D_A'])**2)
        # - cov. matrix of DD: , (master atmo estimated)
        VC = 2*sig0**2 * np.ones(n_ifg)
    Qy = np.diag(VC)
    Qy_i = np.diag(1/np.diag(Qy)) # inverse of diagonal matrix
    # normal equations
    N = (B1.T@Qy_i@B1)
    N_i = np.linalg.inv(N)
    # lambda prepare:
    Z, L, iZt =  lambdaPrepare(Qy, B1, model = model)  
    #% perform VCE only on unique arcs:
    # (use only unique arcs - each PSc once to avoid correlation and bias)
    _,ia = np.unique(arcs[:,0],return_index=True)
    _,ib = np.unique(arcs[ia,1],return_index=True)
    vc_idx = ia[ib]
    VCall = np.zeros_like(arcPhase[vc_idx,:])
    i = 0
    print('Estimating Variance Components...')
    for idx in vc_idx:
        # temporal ambig. solve by bootstrap:
        uw_ph = arcSolveFast(arcPhase[idx,:], Z, L, iZt, N_i, B1, 
                             Qy_i)[1]
        VCall[i,:] = VCE(VC, B1, uw_ph)
        i += 1
    # TODO: remove outliers
    VC_mean = np.mean(VCall, axis=0)
    # SECOND iteration with new VC
    Qy = np.diag(VC_mean)
    Qy_i = np.diag(1/np.diag(Qy))
    N = (B1.T@Qy_i@B1)
    N_i = np.linalg.inv(N)
    Z, L, iZt =  lambdaPrepare(Qy, B1, model = model)
    a_arcs = np.zeros_like(arcPhase)
    uw_ph = np.zeros_like(arcPhase)
    x = np.zeros((arcPhase.shape[0],B1.shape[1]))
    res = np.zeros_like(arcPhase)
    var = np.zeros(arcPhase.shape[0])
    print('Solving '+str(arcPhase.shape[0])+' arcs...')
    for idx in np.arange(arcPhase.shape[0]):
        # temporal ambig. solve:
        (a_arcs[idx,:], uw_ph[idx,:], x[idx], res[idx,:], var[idx]
         ) = arcSolveFast(arcPhase[idx,:], Z, L, iZt, N_i, B1, Qy_i)
    
    # assign to network dataset:
    # if exists, delete:
    if 'arcPhase_uw' in network:
        for k in network.keys():
            if k != 'arcs': 
                del network[k] 
    # - attributes:
    network.attrs['dates'] = dates.datestr2num(stack['slcs'][:].astype('str'))
    network.attrs['masterDate'] = dates.datestr2num(stack.attrs['masterDate'])
    network.attrs['m2ph'] = m2ph
    network.attrs['n_ps'] = n_ps
    # - data:
    network['a_arcs'] = a_arcs
    network['arcPhase'] = arcPhase
    network['arcPhase_uw'] = uw_ph
    network['var'] = var # aposteriori variance factors;
    network['stdRes'] = np.std(res, axis=1)/-m2ph*1e3 # standard deviation of residuals [mm]:
    network['arc_parms'] = x # estimated arc parameters:
    network['VC_mean'] = VC_mean # mean variance components:
    network['B1'] = B1 # design matrix:
        
    # plot ambiguities as sparse matrix:
    if plotFlag:
        plt.spy(a_arcs,aspect='auto')
    print('Temporal ambiguities resolved.')
    #return data


def removePSC(idx, arcs, badPsc):
    goodIdx = np.setdiff1d(idx, badPsc)
    # fix arcs indexing
    i = 0
    for p in badPsc:
        arcs[arcs > p-i] -= 1
        i += 1
    # new indices:
    ps_idx = np.arange(goodIdx.size)
    return goodIdx, ps_idx, arcs


def removeArc(arcs, arcIdx):
    goodIdx = np.setdiff1d(np.arange(arcs.shape[0]), 
                           arcIdx)
    arcs = arcs[goodIdx]
    return goodIdx, arcs


def remove_outliers(data):
    #if not 'psc2' in data:
    #    data.copy('psc','psc2')
    #network = data['network']
    arcs = data['network/arcs'][:]
    stdRes = data['network/stdRes'][:]
    ps_idx = np.arange(data['psc/azimuth'].shape[0])
    #psc = data['psc2']
    # store initial arcs:
    #if not 'arcs_initial' in network:
    #    network['arcs_initial'] = network['arcs'][:].copy()
    # allocate:
    good_ps_idx = np.arange(ps_idx.size)
    good_arc_idx = np.arange(arcs.shape[0])    
    # errorneous arc detection
    # based on standard deviation of residuals
    # with threshold using chi2 confidence interval
    chi2 = ss.chi.fit(stdRes)
    stdThr = ss.chi.interval(0.99, chi2[0], loc=chi2[1], scale=chi2[2])[1]
    badIdx = np.nonzero(stdRes > stdThr)[0]
    # remove bad arcs
    if badIdx.size > 0:
        good_arc_idx, arcs = removeArc(arcs, badIdx)
        # report number of outlier arcs:
        print(badIdx.size,' outlier arcs removed.\n')
    # remove PSc not connected by any arc
    badPsc = np.setdiff1d(ps_idx, arcs.flatten())
    if badPsc.size > 0:
        good_ps_idx, ps_idx, arcs = removePSC(ps_idx, arcs, badPsc)   
        #removePSC(psc, network, badPsc)
        print(badPsc.size,' isolated PSc removed.\n')
    # remove PSc forming isolated connection
    count = np.bincount(arcs.flatten())
    solePsc = np.nonzero(count < 2)[0]
    # allocate:
    good_ps_idx2 = np.arange(good_ps_idx.size)
    good_arc_idx2 = np.arange(good_arc_idx.size)    
    if solePsc.size > 0:
        # find corresponding arcs
        arcIdx = np.unique(
            np.unravel_index(np.nonzero(solePsc[:,None] == 
                                             arcs.flatten())[1],
                                  arcs.shape)[0]
            )
        good_ps_idx2, ps_idx, arcs = removePSC(ps_idx, arcs, solePsc)
        good_arc_idx2, arcs = removeArc(arcs, arcIdx)
        print(solePsc.size,' PSc connected by sole arcs removed.\n')
    # new dataset without outliers:
    good_ps_idx = good_ps_idx[good_ps_idx2]
    good_arc_idx = good_arc_idx[good_arc_idx2]
    updateHDF(data, ps_idx, good_ps_idx, arcs, good_arc_idx)


def updateHDF(data, ps, ps_idx, arcs, arc_idx):
    if 'psc2' in data:
        del data['psc2']
    for k, v in data['psc'].items():
        data.create_dataset('psc2/'+k, data=v[ps_idx])
    data['psc2/idx'] = ps
    data['psc2/psc_idx'] = ps_idx # save former indices
    if 'network2' in data:
        del data['network2']    
    network = data['network']
    data['network2/arcs'] = arcs
    data['network2/a_arcs'] = network['a_arcs'][arc_idx]
    data['network2/arcPhase'] = network['arcPhase'][arc_idx]
    data['network2/arcPhase_uw'] = network['arcPhase_uw'][arc_idx]
    data['network2/var'] = network['var'][arc_idx]
    data['network2/stdRes'] = network['stdRes'][arc_idx]
    data['network2/arc_parms'] = network['arc_parms'][arc_idx]
    data['network2/B1'] = network['B1']
    data['network2/VC_mean'] = network['VC_mean']
    #data['network2/dates'] = network['dates']
    #data['network2/m2ph'] = network['m2ph']
    #data['network2/masterDate'] = network['masterDate']
    for k, v in network.attrs.items():
        data['network2'].attrs[k] = v
    data['network2'].attrs['n_ps'] = ps.size


def updateHDF2(data, badPsc):
      
    ps_idx = data['psc2/idx'][:]
    arcs = data['network2/arcs'][:]
    # find corresponding arcs
    arcIdx = np.unique(
        np.unravel_index(np.nonzero(badPsc[:,None] == 
                                         arcs.flatten())[1],
                              arcs.shape)[0]
        )
    good_arc_idx, arcs = removeArc(arcs, arcIdx)
    good_ps_idx, ps_idx, arcs = removePSC(ps_idx, arcs, badPsc)     
    # temp copies:
    data.move('psc2','psc3')
    data.move('network2','network3')
    # new psc:
    for k, v in data['psc3'].items():
        data.create_dataset('psc2/'+k, data=v[good_ps_idx])
    data['psc2/idx'][:] = ps_idx   
    # new network:
    network = data['network3']
    data['network2/arcs'] = arcs
    data['network2/a_arcs'] = network['a_arcs'][good_arc_idx]
    data['network2/arcPhase'] = network['arcPhase'][good_arc_idx]
    data['network2/arcPhase_uw'] = network['arcPhase_uw'][good_arc_idx]
    data['network2/var'] = network['var'][good_arc_idx]
    data['network2/stdRes'] = network['stdRes'][good_arc_idx]
    data['network2/arc_parms'] = network['arc_parms'][good_arc_idx]
    data['network2/B1'] = network['B1']
    data['network2/VC_mean'] = network['VC_mean']
    for k, v in network.attrs.items():
        data['network2'].attrs[k] = v
    data['network2'].attrs['n_ps'] = ps_idx.size
    # delete temporary:
    del data['network3']
    del data['psc3']


def setRefPoint(psc, network, method = 'auto', plotFlag = 1, **kwargs):
    # parse data:
    arcs = network['arcs'][:]
    stdRes = network['stdRes'][:]
    plh = geoUtils.xyz2plh(psc['xyz'][:])
    #
    if method == 'auto':
        # first 50 PS closest to middle:
        candIdx = np.argsort(np.sum(np.abs(plh[:,:2] 
                                           - np.array([np.mean(plh[:,0]),
                                                       np.mean(plh[:,1])])), 
                                    axis=1))[:50]
        # their average std:
        candStd = np.zeros(candIdx.shape)
        i = 0
        for idx in candIdx:
            arcIdx = np.unravel_index(np.nonzero(idx == 
                                                 arcs.flatten())[0],
                                      arcs.shape)[0]
            candStd[i] = np.mean(stdRes[arcIdx])
            i += 1   
        # take best:
        refIdx = candIdx[np.argmin(candStd)]
    #
    elif method == 'constraint':
        refLat = kwargs['refLat']
        refLon = kwargs['refLon']
        refIdx = np.argmin(np.sum(np.abs(plh[:,:2] 
                                 - np.array([refLat,refLon])/180*np.pi), 
                          axis=1))
    #
    elif method == 'free':
        refIdx = np.nan
    else:
        print('Unknown reference method')
        raise
    # store:
    network.attrs['refIdx'] = refIdx
    if ~np.isnan(refIdx):
        network.attrs['refAz'] = psc['azimuth'][refIdx]
        network.attrs['refR'] = psc['range'][refIdx]
        if plotFlag:
            plotUtils.plot_ref_point(psc, network)
    return refIdx


def network_cond(data):
    """check network conditioning
    """
    # parse data:
    network = data['network2']
    arcs = network['arcs'][:]
    n_arcs, n_ifgs =  network['a_arcs'].shape
    n_ps = network.attrs['n_ps']
    refIdx = network.attrs['refIdx']
    # Design matrix for ambiguity testing and spatial unwraping:
    A = np.zeros((n_arcs,n_ps))
    A[np.arange(n_arcs), arcs[:,1]] = +1
    A[np.arange(n_arcs), arcs[:,0]] = -1
    if np.isnan(refIdx):
        refIdx = 0 # use first point for condtest
    A = np.delete(A, refIdx, axis=1)
    # test conditioning:
    nullA = sl.null_space(A)
    if nullA.size > 0:
        # non-zero elements of null basis are linearly dependant collums:
        depIdx = np.where(np.abs(nullA) > 1e-10)[0]
        depIdx[depIdx >= refIdx] += 1
        # if linearly-dependent -> PSc form an isolated network - remove it:
        #TODO:
        print('Removing isolated network!')
        print('consisting of '+str(len(depIdx))+' points.')
        updateHDF2(data, depIdx)
    else:
        print('Network well-conditioned.')
        


def spatialAmbiguity(network):
    """spatial ambiguity resolution by LSQ  
    """
    # parse data:
    arcs = network['arcs'][:]
    a_arcs = network['a_arcs'][:]
    arcPhase = network['arcPhase'][:]
    n_arcs, n_ifgs = a_arcs.shape
    n_ps = network.attrs['n_ps']
    refIdx = network.attrs['refIdx']
    # Design matrix for ambiguity testing and spatial unwraping:
    A = np.zeros((n_arcs,n_ps))
    A[np.arange(n_arcs), arcs[:,1]] = +1
    A[np.arange(n_arcs), arcs[:,0]] = -1
    # ambiguity weighting matrix:
    var = network['var']/np.max(network['var'])
    Q_a = np.diag(var**2)
    Q_a_i = np.diag(1/np.diag(Q_a)) # inverse
    # Regularize ==> choose one reference PSC if ref point specified
    # free network otherwise
    if np.isnan(refIdx):
        # allocate:
        a_ps = np.zeros((n_ps,n_ifgs))
        # normal equations:
        rhs = A.T@Q_a_i
        N = rhs@A
        N_i = np.linalg.pinv(N)
    else:
        # allocate:
        a_ps = np.zeros((n_ps-1,n_ifgs))
        A = np.delete(A, refIdx, axis=1)
        # test conditioning:
        nullA = sl.null_space(A)
        if nullA.size > 0:
            # non-zero elements of null basis are linearly dependant collums:
            depIdx = np.where(np.abs(nullA) > 1e-10)[0]
            depIdx[depIdx >= refIdx] += 1
            # if linearly-dependent -> PSc form an isolated network - remove it:
            print('Remove isolated network using .network_cond!')
        # normal equations:
        rhs = A.T@Q_a_i
        N = rhs@A
        N_i = np.linalg.inv(N)
    # VCM of residuals:
    #Q_res_diag = np.diag(Q_a) - np.diag(A@N_i@A.T)
    #% unwrap per epoch:
    for t in np.arange(n_ifgs):
        # counters:
        exit_flag = 0
        counter = 0
        skip_idx = 0
        former_idx = np.array([])
        former_OMT = 1e10
        y = a_arcs[:,t].copy() # observations = arc. ambigs.
        while exit_flag == 0 and counter < n_ps/10:
            # float ambiguities:
            a_t = N_i@(rhs@y)
            # residuals:
            res = y - A@a_t
            # Overall model test per interferogram (must be 0!)
            OMT = res@Q_a_i@res
            #print(OMT)
            if np.abs(OMT) > 1e-10: # numerical errors
                if OMT > former_OMT:
                    print('Could not converge with 0 OMT.')
                    exit_flag = 1
                # get max residual
                if skip_idx == 0:
                    fix_idx = np.argmax(np.abs(res))
                else:
                    bad_idx = np.argsort(-np.abs(res)) # minus for descending order
                    fix_idx = bad_idx[skip_idx]
                if fix_idx in former_idx: # if already corrected res.
                    skip_idx += 1
                # fix ambiguity:
                if np.round(np.abs(res[fix_idx])) >= 1:
                    y[fix_idx] -= np.round(res[fix_idx])
                elif res[fix_idx] > 0:
                    y[fix_idx] -= 1
                elif res[fix_idx] < 0:
                    y[fix_idx] += 1
                counter += 1
                np.append(former_idx, fix_idx)
                former_OMT = OMT.copy()
            else:
                exit_flag = 1    
        # integer ambiguities:
        a_ps[:,t] = np.round(a_t)
        # corrected arcs:
        a_arcs[:,t] = y.copy()
        # log:
        print('{:n} of {:n} interferograms resolved.'.format(t+1,n_ifgs))
        print('{:n} unwrapping errors corected.'.format(counter))
        print('Converged with OMT = {:.2f}'.format(OMT))
    #% end ifg loop
    #% wrapped phase per-ps:
    ps_phase = N_i@(rhs@arcPhase)
    # residuals:
    res = arcPhase - A@ps_phase
    # check if inversion consistent by OMT:
    OMT = np.sum(np.diag(res.T@Q_a_i@res))
    if OMT > 1e-10:
        print('Inversion of wrapped phases w.r.t ref point inconsistent!')
    # unwrap:
    ps_phase_unw = -2*np.pi*a_ps + ps_phase
    # make sure master phase equals zero:
    ps_phase_unw[:,network.attrs['dates'] == network.attrs['masterDate']] = 0
    # write results to network structure:
    network['a_ps'] = a_ps
    network['ps_phase_unw'] = ps_phase_unw
    return network


def solve_network_precise(network, psc, model='seasonal'):
    # get precise h2ph factors - outside
    #getPreciseH2PH(psc, stack)
    # get stack-wide functional model and edit per-point basis:
    B = network['B1'][:]
    h2ph = -psc['h2ph'][:] # careful about "-"
    Qy = np.diag(network['VC_mean'])
    Qy_i = np.diag(1/np.diag(Qy))
    m2ph = network.attrs['m2ph']
    # allocate:
    n_ps,n_ifgs = network['a_ps'].shape
    ps_phase = network['ps_phase_unw'][:]
    parms = np.zeros((n_ps,B.shape[1]))
    sig_parms = np.zeros_like(parms)
    res = np.zeros_like(ps_phase)
    var = np.zeros((n_ps,1))
    for p in np.arange(n_ps):
        # replace h2ph
        B[:,1] = m2ph*h2ph[p]
        N = (B.T@Qy_i@B)
        N_i = np.linalg.inv(N)
        P_T = N_i@B.T@Qy_i # orthogonal projector
        parms[p] = P_T@ps_phase[p,:]
        res[p] = ps_phase[p,:] - B@parms[p]
        var[p] = res[p]@Qy_i@res[p] / (n_ifgs - B.shape[1])
        sig_parms[p] = np.sqrt(var[p] * np.diag(N_i))
    #TODO: force master residuals == 0
    # displacement phase;
    ps_displ = (ps_phase.T - B[:,:2]@(parms[:,:2].T)).T
    print('Model parameters estimated w.r.t ref. PSc.')
    # write to hdf:
    if 'ps_parms' not in network:
        network['ps_parms'] = parms
        network['ps_res'] = res
        network['ps_var'] = var
        network['ps_sig_parms'] = sig_parms
        network['ps_displ'] = ps_displ
    else:
        print('Parameters already estimated!')
        network['ps_parms'][...] = parms
        network['ps_res'][...] = res
        network['ps_var'][...] = var
        network['ps_sig_parms'][...] = sig_parms
        network['ps_displ'][...] = ps_displ


def solve_network(network, model='seasonal'):
    #TODO: model of choice + res.h coefs of individual PS
    n_ps,n_ifgs = network['a_ps'].shape
    ps_phase = network['ps_phase_unw'][:]
    # prepare functional model:
    B = network['B1'][:]
    Qy = np.diag(network['VC_mean'])
    Qy_i = np.diag(1/np.diag(Qy))
    N = (B.T@Qy_i@B)
    N_i = np.linalg.inv(N)
    P_T = N_i@B.T@Qy_i # orthogonal projector
    # allocate:
    parms = np.zeros((n_ps,B.shape[1]))
    sig_parms = np.zeros_like(parms)
    res = np.zeros_like(ps_phase)
    var = np.zeros((n_ps,1))
    for p in np.arange(n_ps):
        parms[p] = P_T@ps_phase[p,:]
        res[p] = ps_phase[p,:] - B@parms[p]
        var[p] = res[p]@Qy_i@res[p] / (n_ifgs - B.shape[1])
        sig_parms[p] = np.sqrt(var[p] * np.diag(N_i))
    #TODO: force master residuals == 0
    # displacement phase;
    ps_displ = (ps_phase.T - B[:,:2]@(parms[:,:2].T)).T
    print('Model parameters estimated w.r.t ref. PSc.')
    # write to hdf:
    if 'ps_parms' not in network:
        network['ps_parms'] = parms
        network['ps_res'] = res
        network['ps_var'] = var
        network['ps_sig_parms'] = sig_parms
        network['ps_displ'] = ps_displ
    else:
        print('Parameters already estimated!')
        network['ps_parms'][...] = parms
        network['ps_res'][...] = res
        network['ps_var'][...] = var
        network['ps_sig_parms'][...] = sig_parms
        network['ps_displ'][...] = ps_displ

def remove_RPN(network):
    if np.isnan(network.attrs['refIdx']): # free network
         RPN = np.mean(network['ps_phase_unw'][:], axis=0)
    else:
         RPN = np.mean(network['ps_res'][:], axis=0)
    RPNarr = np.tile(RPN, (network['ps_phase_unw'].shape[0], 1))
    ph_corr = network['ps_phase_unw'][:] - RPNarr
    network['ps_phase_unw'][...] = ph_corr
    return


def decomp(los, sig_los, inc, head):
    """LOS to vertical/horizontal displacement decomposition
    """
    # reduce to common 0:
    zero_idx = np.argmin(np.sum(np.abs(los), axis=1))
    los = los - los[zero_idx]
    # stack design matrix:    
    A = np.column_stack((np.cos(inc), -np.sin(inc) * np.cos(head + np.pi/2)))
    # LSQ solution:
    x = np.zeros((los.shape[0], 2))
    sig_x = np.zeros_like(x)
    for i in np.arange(los.shape[0]):
        invQy = np.diag(1 / (sig_los[i]**2)) # inverse of diagonal VCM
        x[i] = np.linalg.inv(A.T @ invQy @ A) @ A.T @ invQy @ los[i]
        sig_x[i] = np.sqrt(np.diag(np.linalg.inv(A.T @ invQy @ A)))
    return x.T,sig_x.T
    
# def decomp(asc, dsc, inc_asc, inc_dsc, head_asc, head_dsc, VC_asc, VC_dsc):
#     """LOS to vertical/horizontal displacement decomposition
#     """
#     # design matrix:
#     A = np.array([[np.cos(inc_asc), 
#                    +np.sin(inc_asc)*np.cos(head_asc+np.pi/2)],
#                   [np.cos(inc_dsc), 
#                    +np.sin(inc_dsc)*np.cos(head_dsc+np.pi/2)]
#         ])
#     #x = np.linalg.solve(A,np.array([asc,dsc]))    
#     # VCM solution:
#     x = np.empty((len(asc),2))
#     sig_x = np.empty((len(asc),2))
#     for i in np.arange(len(asc)):
#         y = np.array([asc[i],dsc[i]])
#         Qy = np.diag([VC_asc[i]**2,VC_dsc[i]**2])
#         invQy = np.linalg.inv(Qy)
#         x[i] = np.linalg.inv(A.T@invQy@A)@A.T@invQy@y
#         sig_x[i] = np.sqrt(np.diag(np.linalg.inv(A.T@invQy@A)))
    
#     return x.T,sig_x.T


def KS(amp1,amp2,signif):
    """KS 2-sample test
    """
    return ss.ks_2samp(amp1,amp2)[1] > signif


#%% DS - module:
############################################################################## 

# def windowSHP(amp, pixIdx, pixThr = 20, neighbours = 8, significance = 0.1):
#     """Identify SHP candidates on defined amplitude window using KS 2-sample
#     test at specified significance level.
#     return corresponding SHP indices w.r.t. pixIdx array, starting with
#     centerPoint. If less SHP identified than pidxThr, return only centerPoint.
#     """

#     centerIdx = np.floor(np.asarray(amp[:,:,0].shape)/2).astype('int')
#     P = amp[centerIdx[0],centerIdx[1],:]
#     pIdx = pixIdx[centerIdx[0],centerIdx[1]]
    
#     # flatten in 1 dimension:
#     amp_flat = amp.reshape(-1, amp.shape[-1])
    
#     # find SHP candidates using KS-2-sample test:
#     cands = np.apply_along_axis(KS,1,amp_flat,P,significance)
#     cands = cands.reshape(amp.shape[:2])
    
#     # remove non-neighbouring SHP candidates:
#     # find them using Flood-fill algorithm:
#     cands = np.where(cands, 255, 0).astype(np.uint8)
#     mask = np.zeros(np.asarray(cands.shape)+2, dtype=np.uint8)
#     cv2.floodFill(cands, mask, (centerIdx[1],centerIdx[0]), 100, flags=neighbours)
#     mask = mask[1:-1, 1:-1]
#     #plt.imshow(mask)
#     #plt.figure()
#     #plt.imshow(cands); plt.colorbar(); plt.title('mask')
#     mask[centerIdx[0],centerIdx[1]] = 0 # remove point itself
#     # store indices:
#     SHPidx = pixIdx[np.nonzero(mask)]
#     # return 1-d array with pIdx on [0] and all SHP indices as [1:]
#     # check if number of SHP > 20:
#     if SHPidx.size > pixThr:
#         return np.hstack((pIdx,SHPidx))
#     else:
#         return pIdx

# def prepareStrides(ampArray, azPatchSize = 9, rPatchSize = 15):
#     """take full amplitude array [az,r,t], azimuth and range window sizes
#     return strided patches of amplitude and indices for parallel processing
#     """
#     # prepare array of original indices:
#     fullSize = ampArray[:,:,0].size
#     fullShape = ampArray[:,:,0].shape
#     idxArray = np.reshape(np.arange(fullSize),fullShape)
#     # reorder so that temporal dim. is first axis
#     img = ampArray.transpose(2,0,1)
#     # shape of strided array    
#     shape = (img.shape[0], img.shape[1] - azPatchSize + 1, 
#              img.shape[2] - rPatchSize + 1, 
#              azPatchSize, rPatchSize)
#     # it's new strides [t,az,r,az,r]:
#     strides = (img.strides[0],) + 2 * img.strides[1:]
#     patches = np.lib.stride_tricks.as_strided(img, shape=shape, 
#                                               strides=strides)
#     # re-order patches
#     patches = patches.transpose(1,2,3,4,0).reshape(-1,azPatchSize,rPatchSize,img.shape[0])
#     # do the striding also for array of indices:
#     shape = (idxArray.shape[0] - azPatchSize + 1, idxArray.shape[1] - rPatchSize + 1, 
#              azPatchSize, rPatchSize)
#     strides = 2 * idxArray.strides
#     idxPatches = np.lib.stride_tricks.as_strided(idxArray, 
#                                               shape=shape, strides=strides)
#     idxPatches = idxPatches.reshape(-1, azPatchSize, rPatchSize)
#     return patches, idxPatches

#%% GNSS module:
############################################################################## 
    
# class GNSS_InSAR:
#     def loadPOS(inFile):
#         t = np.empty((0, 1))
#         xyz = np.empty((0, 3))
#         with open(inFile,'r',errors='replace') as f:
#             for i, line in enumerate(f):
#                 if i > 19:
#                     if line.startswith('*'):
#                         break
#                     else:
#                         q = line.split()
#                         t = np.append(t,
#                                       dates.datestr2num(q[0]+q[1]+q[2]))
#                         xyz = np.append(xyz,
#                                         [np.array([float(q[4]),
#                                                    float(q[5]),
#                                                    float(q[6])])],axis=0)
#         return t,xyz
    
#     def loadIGS(inFile):
#         t = np.empty((0, 1))
#         xyz = np.empty((0, 3))
#         with open(inFile,'r',errors='replace') as f:
#             for i, line in enumerate(f):
#                 q = line.split()
#                 qq = datetime.datetime.strptime(q[1],'%y%b%d')
#                 t = np.append(t,dates.date2num(qq))
#                 xyz = np.append(xyz,
#                                 [np.array([float(q[3]),
#                                            float(q[4]),
#                                            float(q[5])])],axis=0)
#         return t,xyz
    
#     def loadStationPOS(inDir):
#         files = glob.glob(inDir+os.sep+'*.pos')    
#         t = np.empty(0)
#         xyz = np.empty((0, 3))
#         for f in files:
#             (t_f,xyz_f) = GNSS_InSAR.loadPOS(f)
#             t = np.concatenate((t,t_f))
#             xyz = np.concatenate((xyz,xyz_f))
#         # sort it:
#         idx = np.argsort(t)
#         xyz = xyz[idx,:]
#         t = t[idx]
#         return t,xyz
    
#     def plot_GNSS_nev(t,nev,stn,outDir):
#         formatter = dates.DateFormatter('%m-%y')
#         labels = ['n [mm]','e [mm]','v [mm]'] 
#         fig,axes = plt.subplots(3,1,figsize=(8,6),dpi=120)
#         for i in [0,1,2]:
#             axes[i].plot(t,nev[i,:]*1e3)
#             start, end = axes[i].get_xlim()
#             stepsize = (end+60-start)/10
#             axes[i].xaxis.set_ticks(np.arange(start, end+60, stepsize))
#             axes[i].xaxis.set_major_formatter(formatter)
#             axes[i].set_ylabel(labels[i],fontsize=14)
#         fig.suptitle(stn.upper())
#         plt.subplots_adjust(hspace=0.5)
#         plt.savefig(outDir+stn+'_GNSS_nev.png', bbox_inches='tight')
#         plt.close()
    
#     def loadCSV_insar(inCSV):
#         # read as pandas dataframe:
#         ts = pd.read_csv(inCSV,header=0)
#         # drop dpplicates in case of sarproz:
#         dupl = [col for col in list(ts.columns) if '.1' in col]
#         ts = ts.drop(columns = dupl)
        
#         insarDates = list(ts.columns[16:])
#         try:
#             masterIdx = np.where(ts.iloc[0,:][16:] == 0)[0][0]
#             masterDate = dates.datestr2num(insarDates[masterIdx])
#         except:
#             masterIdx = np.nan    
#             masterDate = np.nan
#         return (ts,insarDates,masterIdx,masterDate)
    
#     def intersect_mtlb(a, b):
#         a1, ia = np.unique(a, return_index=True)
#         b1, ib = np.unique(b, return_index=True)
#         aux = np.concatenate((a1, b1))
#         aux.sort()
#         c = aux[:-1][aux[1:] == aux[:-1]]
#         return c, ia[np.isin(a1, c)], ib[np.isin(b1, c)]
    
#     def est_velocity_InSAR(insar):
#         dt = (insar['t'] - insar['t'][0])/365.25
#         A = np.concatenate((np.ones((dt.size,1)),np.array([dt]).T),1)
#         vel,OMT = np.linalg.lstsq(A,insar['dLOS'],rcond=None)[:2]
#         var = OMT/(A.shape[0]-A.shape[1])*np.linalg.inv(A.T@A)
#         insar['vel'] = vel[1]
#         insar['sig_vel'] = np.sqrt(var[1,1])
#         return insar
    
#     def est_velocity_GNSS(gnss,masterDate):
#         t = gnss['t']
#         refDateIdx = np.argmin(np.abs(t-masterDate))
#         dt = (t - t[refDateIdx])/365.25
#         A = np.concatenate((np.ones((dt.size,1)),np.array([dt]).T),1)
#         vel,OMT = np.linalg.lstsq(A,gnss['los'],rcond=None)[:2]
#         var = OMT/(A.shape[0]-A.shape[1])*np.linalg.inv(A.T@A)
#         gnss['vel'] = vel[1]
#         gnss['sig_vel'] = np.sqrt(var[1,1])
#         return gnss
    
#     def plotGNSS_InSAR_vertical(gnss_asc,insar_asc,gnss_dsc,insar_dsc,outDir):
#         formatter = dates.DateFormatter('%m-%y')
#         fig,axes = plt.subplots(2,1,figsize=(8,6),dpi=120)
#         sig = 2.5*gnss_asc['sig_vel']
#         lab = ('GNSS-DD [VEL = ' + f"{gnss_asc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[0].plot(gnss_asc['t'],gnss_asc['nev'][2]*1e3,
#                      label=lab,linewidth=0.5)
#         sig = 2.5*insar_asc['sig_vel']
#         lab = ('InSAR-DD [VEL = ' + f"{insar_asc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[0].plot(insar_asc['t'],insar_asc['v'],
#                      label=lab,linewidth=2)
#         #axes[0].set_title('Ascending 141')
#         sig = 2.5*gnss_dsc['sig_vel']
#         lab = ('GNSS-DD [VEL = ' + f"{gnss_dsc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[1].plot(gnss_dsc['t'],gnss_dsc['nev'][2]*1e3,
#                      label=lab,linewidth=0.5)
#         sig = 2.5*insar_dsc['sig_vel']
#         lab = ('InSAR-DD [VEL = ' + f"{insar_dsc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[1].plot(insar_dsc['t'],insar_dsc['v'],
#                      label=lab,linewidth=2)
#         #axes[1].set_title('Descending 46')
#         # decide ylim:
#         ymin = np.min([axes[0].get_ylim()[0],axes[1].get_ylim()[0]])
#         ymax = np.max([axes[0].get_ylim()[1],axes[1].get_ylim()[1]])
#         # quality:
#         orbit = ['Ascending 141','Descending 46']
#         quality = [insar_asc['coh'],insar_dsc['coh']]
#         idx = 0
#         for ax in axes:
#             start, end = ax.get_xlim()
#             stepsize = (end+60-start)/10
#             ax.xaxis.set_ticks(np.arange(start, end+60, stepsize))
#             ax.xaxis.set_major_formatter(formatter)
#             ax.set_ylim((ymin,ymax))
#             ax.set_ylabel('Apparent vertical[mm]',fontsize=12)
#             #ax.annotate('Quality = '+f'{quality[idx]:.2f}',
#             #            xy=(ax.get_xlim()[0]+60,ymin+(ymax-ymin)/10),
#             #            fontSize = 10)
#             ax.legend()
#             ax.grid(alpha = 0.3)
#             ax.set_title(orbit[idx] + f' [Quality = {quality[idx]:.2f}]')
#             idx += 1
#         fig.suptitle(gnss_asc['stn'].upper(),verticalalignment='bottom',
#                      fontweight='bold')
#         plt.subplots_adjust(hspace=0.5)
#         plt.savefig(outDir+gnss_asc['stn']+'_GNSS_InSAR_vertical.png', 
#                     bbox_inches='tight')
#         plt.close()
    
#     def plotGNSS_InSAR(gnss_asc,insar_asc,gnss_dsc,insar_dsc,outDir):
#         formatter = dates.DateFormatter('%m-%y')
#         fig,axes = plt.subplots(2,1,figsize=(8,6),dpi=120)
#         sig = 2.5*gnss_asc['sig_vel']
#         lab = ('GNSS-DD [VEL = ' + f"{gnss_asc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[0].plot(gnss_asc['t'],gnss_asc['los'],
#                      label=lab,linewidth=0.5)
#         sig = 2.5*insar_asc['sig_vel']
#         lab = ('InSAR-DD [VEL = ' + f"{insar_asc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[0].plot(insar_asc['t'],insar_asc['dLOS'],
#                      label=lab,linewidth=2)
#         #axes[0].set_title('Ascending 141')
#         sig = 2.5*gnss_dsc['sig_vel']
#         lab = ('GNSS-DD [VEL = ' + f"{gnss_dsc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[1].plot(gnss_dsc['t'],gnss_dsc['los'],
#                      label=lab,linewidth=0.5)
#         sig = 2.5*insar_dsc['sig_vel']
#         lab = ('InSAR-DD [VEL = ' + f"{insar_dsc['vel']:.1f} $\pm$ " +  
#                f"{sig:.1f} mm/yr]")
#         axes[1].plot(insar_dsc['t'],insar_dsc['dLOS'],
#                      label=lab,linewidth=2)
#         #axes[1].set_title('Descending 46')
#         # decide ylim:
#         ymin = np.min([axes[0].get_ylim()[0],axes[1].get_ylim()[0]])
#         ymax = np.max([axes[0].get_ylim()[1],axes[1].get_ylim()[1]])
#         # quality:
#         orbit = ['Ascending 141','Descending 46']
#         quality = [insar_asc['coh'],insar_dsc['coh']]
#         idx = 0
#         for ax in axes:
#             start, end = ax.get_xlim()
#             stepsize = (end+60-start)/10
#             ax.xaxis.set_ticks(np.arange(start, end+60, stepsize))
#             ax.xaxis.set_major_formatter(formatter)
#             ax.set_ylim((ymin,ymax))
#             ax.set_ylabel('dLOS [mm]',fontsize=14)
#             #ax.annotate('Quality = '+f'{quality[idx]:.2f}',
#             #            xy=(ax.get_xlim()[0]+60,ymin+(ymax-ymin)/10),
#             #            fontSize = 10)
#             ax.legend()
#             ax.grid(alpha = 0.3)
#             ax.set_title(orbit[idx] + f' [Quality = {quality[idx]:.2f}]')
#             idx += 1
#         fig.suptitle(gnss_asc['stn'].upper(),verticalalignment='bottom',
#                      fontweight='bold')
#         plt.subplots_adjust(hspace=0.5)
#         plt.savefig(outDir+gnss_asc['stn']+'_GNSS_InSAR.png', bbox_inches='tight')
#         plt.close()
    
#     def extractPS(ts,insarDates,GNSS,cohThr):
#         # coherence threshold on whole pd dataframe:
#         ts = ts[ts['COHER'] > cohThr]
#         InSAR = []
#         for stn in GNSS:   
#             lonDif = np.abs(ts["LON"] - stn['plh'][1]*180/np.pi)
#             latDif = np.abs(ts["LAT"] - stn['plh'][0]*180/np.pi)
#             # TODO: add warning if too far!, or make threshold argument
#             idx = np.argmin(lonDif+latDif)
#             InSAR.append({'stn': stn['stn'],
#                           't': dates.datestr2num(insarDates),
#                           'LOS': np.array(ts.iloc[idx,:][16:]),
#                           'coh': ts.iloc[idx,:][10]
#                           })
#         return InSAR
    
#     def gnss2los(GNSS_in,refIdx,masterDate,orbit):
#         GNSS_out = copy.deepcopy(GNSS_in)
#         for stn in GNSS_out:
#             # find date overlap
#             t, ia, ib = GNSS_InSAR.intersect_mtlb(stn['t'], 
#                                                   GNSS_in[refIdx]['t'])    
#             stn['dxyz'] = stn['xyz'][ia,:] - GNSS_in[refIdx]['xyz'][ib,:]
#             refDateIdx = np.argmin(np.abs(t-masterDate))
#             stn['dxyz'] = stn['dxyz'] - stn['dxyz'][refDateIdx,:]
#             stn['nev'] = geoUtils.xyz2nev(stn['dxyz'],stn['plh'])
#             stn['t'] = t
#             # project nev to LOS:
#             if orbit == 'asc':
#                 stn['los'] = geoUtils.nev2los(stn['nev'].T,stn['heading_asc'],
#                                               stn['incidence_asc'])*1e3
#             else:
#                 stn['los'] = geoUtils.nev2los(stn['nev'].T,stn['heading_dsc'],
#                                   stn['incidence_dsc'])*1e3    
#         return GNSS_out
    
#     def insarDD(InSAR,refIdx):
#         for stn in InSAR:
#             stn['dLOS'] = stn['LOS'] - InSAR[refIdx]['LOS']
#         return InSAR

#%% backup HDF
# def recursively_save_dict_contents_to_group( h5file, path, dic):

#     # argument type checking
#     if not isinstance(dic, dict):
#         raise ValueError("must provide a dictionary")        

#     if not isinstance(path, str):
#         raise ValueError("path must be a string")
#     if not isinstance(h5file, h5py._hl.files.File):
#         raise ValueError("must be an open h5py file")
#     # save items to the hdf5 file
#     for key, item in dic.items():
#         #print(key,item)
#         key = str(key)
#         if isinstance(item, list):
#             item = np.array(item)
#             #print(item)
#         if not isinstance(key, str):
#             raise ValueError("dict keys must be strings to save to hdf5")
#         # save strings, numpy.int64, and numpy.float64 types
#         if isinstance(item, (np.int64, np.float64, str, np.float, float, 
#                              np.float32,int)):
#             #print( 'here' )
#             h5file[path + key] = item
#         elif isinstance(item,type(None)):
#             h5file[path + key] = np.nan
#         # save numpy arrays
#         elif isinstance(item, np.ndarray):            
#             try:
#                 h5file[path + key] = item
#             except:
#                 item = np.array(item).astype('|S9')
#                 h5file[path + key] = item
#         # save dictionaries
#         elif isinstance(item, dict):
#             recursively_save_dict_contents_to_group(h5file, path + key + '/', item)
#         # other types cannot be saved and will result in an error
#         else:
#             #print(item)
#             raise ValueError('Cannot save %s type.' % type(item))    

#%% backup APS
# def cov_model(d, parms, model='gaussian', nugget = True):
#     # parms = [psill, range, nugget]
#     if model == 'gaussian':
#         cv = (parms[0]+parms[2])*np.exp(-( (d**2) / ((parms[1]*4/7)**2) ) )
#         if nugget:
#             cv += np.kron(np.eye(d.shape[0]), parms[2])
#     return cv

# def kriging_matrix(x, y, x0, y0, var_model):
#     """construct Kriging system matrix and solve it:
#     # for universal kriging with drift of order 1
#     """
#     # matrix for original points:
#     n = x.size
#     xy = np.column_stack((x, y))
#     d = cdist(xy, xy, "euclidean")
#     A = np.zeros((n + 3, n + 3))
#     A[:n, :n] = cov_model(d, var_model)
#     A[:n, n] = 1.0
#     A[n, :n] = 1.0
#     A[:n, n+1:] = 0.0001*xy # normalisation to avoid singular matrix
#     A[n+1:, :n] = 0.0001*xy.T
#     # matrix for interpolant:
#     n0 = x0.size
#     xy0 = np.column_stack((x0, y0))
#     d = cdist(xy, xy0, "euclidean")
#     A0 = np.zeros((n + 3, n0))
#     A0[:n, :] = cov_model(d, var_model, nugget=False)
#     A0[n, :] = 1
#     A0[n+1:, :] = 0.0001*xy0.T
#     # solve system:
#     q = np.linalg.solve(A, A0)
#     return q[:n,:]

# def kriging(x, y, z, x0, y0, var_model):
#     # params:
#     dx_max = 1e5 # max distance
#     n_max = int(1e2) # max number of neighbours    
#     # iterate points:
#     sol = np.zeros_like(x0)
#     for i in np.arange(x0.size):
#         # PS coords:
#         x0_ = x0[i].copy()
#         y0_ = y0[i].copy()
#         # initialize:
#         n_x = int(x.size)
#         n_max = min(n_max, n_x)
#         # use only max. neighbours up to max. distance
#         dx = np.hypot(x - x0_, y - y0_)
#         idx = np.argsort(dx[dx < dx_max])
#         x_ = x[idx[:n_max]]
#         y_ = y[idx[:n_max]]
#         z_ = z[idx[:n_max]]
#         #kriging system matrix:
#         A = kriging_matrix(x_, y_, x0_, y0_, var_model)
#         # solution:
#         sol[i] = A.T@z_
#     return sol
###

def fix_geocoding_CR(data, station_log, process_dir, stack, refStn):
    
    stations, refl_flag = ioUtils.load_stations(station_log, process_dir)
    if not refl_flag:
        print('Perform reflectivity analysis first!')
        return   
    #%% find positioning corrects:
    plh_all = data['psc2/plh'][:]
    azimuth_all = data['psc2/azimuth'][:]
    range_all = data['psc2/range'][:]
    
    dh_hat = data['network2']['ps_parms'][:,1]
    refIdx = data['network2'].attrs['refIdx']
    dh_hat = np.insert(dh_hat, refIdx, 0)
    h_hat = plh_all[:,2] + dh_hat
    # iterate for stns:
    azCorr = np.zeros(len(stations))
    rgCorr = np.zeros_like(azCorr)
    hCorr = np.zeros_like(azCorr)
    Azimuth = np.zeros_like(azCorr)
    Range = np.zeros_like(azCorr)
    i = 0
    for stn in stations:
        stn_plh = stn.get_plh()[0]
        Azimuth[i], Range[i] = radarUtils.plh2radar(stn_plh, stack.masterMetadata)
        idx = np.argmin(cdist(plh_all[:,:2], stn_plh[None,:2]))
        dplh = stn_plh - plh_all[idx]
        #print(dplh)
        azCorr[i] = Azimuth[i] - azimuth_all[idx]
        rgCorr[i] = Range[i] - range_all[idx]
        # SRTM - true difference:
        #dh = dplh[2]
        hCorr[i] = stn_plh[2] - h_hat[idx]
        #print((dAz,dRg, dh))
        i += 1
    #%% fit corrections:
    A = np.column_stack((np.ones_like(Azimuth), 
                         Azimuth-Azimuth[refStn], 
                         Range-Range[refStn]))
    rgFit = np.linalg.lstsq(A, rgCorr, rcond=None)[0]
    azFit = np.linalg.lstsq(A, azCorr, rcond=None)[0]
    hFit = np.linalg.lstsq(A, hCorr, rcond=None)[0]
    
    A_all = np.column_stack((np.ones_like(azimuth_all), 
                         azimuth_all-Azimuth[refStn], 
                         range_all-Range[refStn]))
    rgCorr_all = A_all@rgFit
    azCorr_all = A_all@azFit
    hCorr_all = A_all@hFit 
    #hCorr_all = np.mean(hCorr) #A_all@hFit 
    
    #%% make corrections:
    plh_fix = geoUtils.xyz2plh(radarUtils.radar2xyz(azimuth_all + azCorr_all,
                                 range_all + rgCorr_all,
                                 h_hat + hCorr_all,
                                 stack.masterMetadata))
    
    data['psc2/plh'][...] = plh_fix
    return data, plh_fix