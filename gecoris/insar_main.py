#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Main function to perform InSAR analysis using GECORIS

R. Czikhardt 
4.3.2021
"""

from gecoris import insarUtils, plotUtils

#%% DEFAULT PARAMETERS:

parms = {
    'stackId' : 's1_dsc51',
    'stackDir' : '/data/GUDS/CR_Prievidza/DSC51/',
    'subswath' : 'IW3',
    'startDate' : '20200219',
    #% define aoi:
    'min_lon' : 18.60,
    'max_lon' : 18.74,
    'min_lat' : 48.70,
    'max_lat' : 48.79,
    'aver_h' : 440,         # approximate average ellipsoidal height
    # parameters:
    'D_A_thr' : 0.25,       # Norm. Amplitude Dispersion (NAD) threshold on PS sel.
    'model' : 'seasonal',   # func. model to solve ambiguities, 'linear' / 'seasonal'
    'reference' : 'auto',   # 'auto' / 'free' / 'contraint'
    'APS_flag' : 1,         # 0/1 = estimate and remove Atmospheric Phase Screen (APS)
    'plot_flag': 1,         # plots, 0 = none, 1 = sparse, 2 = detailed
    # outputs:
    'outDir' : '/data/GUDS/CR_Prievidza/insar/'
}

# update default parms by input:
# TODO
#parms.update(dic1)
    
#%% prepare insar data in HDF:
data = insarUtils.prepare_insar(parms)
    
#%% or open existing:
#HDFfile = '/data/gecoris_test/insar/insar_DSC51.hdf5'
#data = insarUtils.openHDF(HDFfile)

#%% plot PSC
if parms['plot_flag']:
    plotUtils.plot_psc(data['psc'], parms['outDir'] + 'psc_D_A.png')

#%% create network:
data['network/arcs'] = insarUtils.createNetwork(data['psc'], 
                                                parms['plot_flag'])

#%% temporal ambig:
insarUtils.temporalAmbiguity(data['stack'],
                             data['psc'],
                             data['network'], model='seasonal')
if parms['plot_flag']:
    plotUtils.plot_network_quality(data['network'])

#%% outlier detection:
insarUtils.remove_outliers(data)

#%% ref. point:
insarUtils.setRefPoint(data['psc2'], data['network2'], 
                       method='auto') # TODO: change to parms
    
#%% spatial ambiguity:    
insarUtils.spatialAmbiguity(data['network2'])
    
#%% network solution
insarUtils.solve_network(data['network2'])

#%% plot it:
if parms['plot_flag']:
    plotUtils.plot_network_parms(data['network2'], data['psc2'], 
                                 parms['outDir']+'/parms_1st.png')

#%% APS estimation:
# TODO: aps_flag
insarUtils.APS(data['psc2'], data['network2'], data['stack'], data['psc'], 
               parms['plot_flag'], apsDir = parms['outDir'] + '/aps/')

#%% 2nd iteration after APS:
insarUtils.temporalAmbiguity(data['stack'], 
                             data['psc'], 
                             data['network2'], model='seasonal')
if parms['plot_flag']:
    plotUtils.plot_network_quality(data['network2'])
#%% 2nd outlier removal:
del data['network']
data['network'] = data['network2']    
insarUtils.remove_outliers(data)
#%% ref. point (former):
data['network2'].attrs['refIdx'] = insarUtils.ref_coords2idx(
    data['psc2'], 
    data['network2'].attrs['refAz'], 
    data['network2'].attrs['refR'])
    
#%% spatial ambiguity:    
insarUtils.spatialAmbiguity(data['network2'])

#%% network solution
insarUtils.solve_network(data['network2'])

#%% plot it:
if parms['plot_flag']:
    plotUtils.plot_network_parms(data['network2'], data['psc2'], 
                                 parms['outDir']+'/parms_2nd.png')

#%% export HDF to CSV:
outCSV = parms['outDir']+'/insar_'+parms['stackId']+'.csv'
insarUtils.HDF2csv(data, outCSV)
# -optionally- convert to shapefile:
#csv2shp(outCSV, outCSV.replace('.csv','.shp')) 

#%% close dataset:
data.close()

